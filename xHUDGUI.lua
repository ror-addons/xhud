-- this file really is a big mess. I should make it more modular, but I'm too lazy to do a complete rewrite AGAIN

LibGUI = LibStub("LibGUI")

local BUTTONS = {
	"General", "Size", "Icon Scale", "Color",
	"Texture", "Alpha", "Filling", "Behaviour",
	"Texts", "Buffs", "Positioning", "Profiles",
				}

local BARLIST = {
	["Player HP"] = "xHUDPlayerHP", ["Player AP"] = "xHUDPlayerAP", ["Career"] = "xHUDPlayerCareer", ["Morale"] = "xHUDPlayerMorale",
	["Friend HP"] = "xHUDFriendlyTargetHP", ["Enemy HP"] = "xHUDHostileTargetHP",
	["Experience"] = "xHUDPlayerXP", ["Renown"] = "xHUDPlayerRP", ["Influence"] = "xHUDPlayerInf", ["Cast Bar"] = "xHUDCastBar",
				}

local ICONLIST = {
	["Friend Career Icon"] = "xHUDFriendlyTargetIcon", ["Enemy Career Icon"] = "xHUDHostileTargetIcon",
				}

local BUFFLIST = {
	["Player Buffs"] = "PlayerBuffs", ["Friend Buffs"] = "FriendlyTargetBuffs", ["Enemy Buffs"] = "HostileTargetBuffs",
					}

local Menu, Panel, Vars
local activePanel
local selectedIndex
local TIME_DELAY = 0.005
local timeLeft = TIME_DELAY -- start in TIME_DELAY seconds
local direction

local function S(str)
	return tostring(str)
end

local function AllSettings()
	local settings = {}

	for i,val in ipairs(xHUD.BuffList) do
		table.insert(settings,xHUD.Settings[val])
	end
	for i,val in ipairs(xHUD.BarList) do
		table.insert(settings,xHUD.Settings[val])
	end
	for i,val in ipairs(xHUD.IconList) do
		table.insert(settings,xHUD.Settings[val])
	end
	for key,val in pairs(xHUD.Settings.Texts) do
		table.insert(settings,val)
	end

	return settings	
end

function xHUD.ShowGUI()
	if not Menu then
		xHUD.CreateGUI()
	end
	Menu:Show()
end

function xHUD.MenuButtonPressed(i)
	if not Panel.Containers[i] then
		xHUD.CreateContainer(i)
	end
	if activePanel then
		Panel.Containers[activePanel]:Hide()
		if activePanel == i then
			activePanel = nil
			Panel:Hide()
			return
		end
	end
	activePanel = i
	selectedIndex = nil
	Panel.Containers[activePanel]:Show()
	Panel.Title:SetText(BUTTONS[i])
	Panel:Resize(500,60+80+Panel.Containers[activePanel].height)
	Panel:Show()
end

function xHUD.OnSelChangedFrame(index)
	local Input = Panel.Containers[activePanel].Input
	local Label = Panel.Containers[activePanel].Label
	local selected = S(Input.Frame:Selected())
	local settings

	if BUTTONS[activePanel] == "General" then
		if selected == "apply to all" then
			settings = {enabled = true,}
		elseif ICONLIST[selected] then
			settings = xHUD.Settings[ICONLIST[selected]]
		else
			settings = xHUD.Settings[BARLIST[selected]]
		end
	elseif BUTTONS[activePanel] == "Texts" then
		settings = xHUD.Settings.Texts[selected]
	elseif BUTTONS[activePanel] == "Icon Scale" then
		settings = xHUD.Settings[ICONLIST[selected]]
	elseif BUTTONS[activePanel] == "Buffs" then
		if selected == "apply to all" then
			settings = {
				enabled = true,
				size = "",
				direction1 = "up",
				direction2 = "left",
				max = 8,
				alpha = "",
				ooc_alpha = "",
				no_target_fade = true,
				clickthrough = false,
				selfbuff = false,
				arc = 0,
				}
		else
			settings = xHUD.Settings[BUFFLIST[selected]]
		end
	elseif BUTTONS[activePanel] == "Positioning" then
		if selected == "apply to all" then
			Input.X:Hide()
			Input.Y:Hide()
			Label.X:Hide()
			Label.Y:Hide()
			settings = {
				x = "",
				y = "",
				layouteditor = false,
			}
		else
			Input.X:Show()
			Input.Y:Show()
			Label.X:Show()
			Label.Y:Show()
			if BARLIST[selected] then
				settings = xHUD.Settings[BARLIST[selected]]
			elseif BUFFLIST[selected] then
				settings = xHUD.Settings[BUFFLIST[selected]]
			elseif ICONLIST[selected] then
				settings = xHUD.Settings[ICONLIST[selected]]
			else
				settings = xHUD.Settings.Texts[selected:match("(.+) %[Text%]")]
			end
		end 
	elseif BUTTONS[activePanel] == "Profiles" then
		settings = {}
	else
		if selected == "apply to all" then
			settings = {
				enabled = true,
				width = "",
				height = "",
				padding = "",
				color = {"","",""},
				bgcolor = {"","",""},
				alpha = "",
				ooc_alpha = "",
				bgalpha = "",
				no_target_fade = true,
				direction = "up",
				clickthrough = false,
				smooth = false,
				inverse = false,
				}
		else
			settings = xHUD.Settings[BARLIST[selected]]
		end
	end
	
	if BUTTONS[activePanel] == "General" then
		Input.Enabled:SetValue(settings.enabled)
	elseif BUTTONS[activePanel] == "Size" then	
 	Input.Width:SetText(settings.width)
			Input.Height:SetText(settings.height)
			Input.Padding:SetText(settings.padding)
	elseif BUTTONS[activePanel] == "Icon Scale" then
	Input.Scale:SetText(settings.scale)
	elseif BUTTONS[activePanel] == "Color" then
		Input.ColorR:SetText(settings.color[1])
		Input.ColorG:SetText(settings.color[2])
		Input.ColorB:SetText(settings.color[3])
		Input.BgColorR:SetText(settings.bgcolor[1])
		Input.BgColorG:SetText(settings.bgcolor[2])
		Input.BgColorB:SetText(settings.bgcolor[3])
		if selected == "Career" then
			Label.Color2:Show()
			Input.Color2R:Show()
			Input.Color2G:Show()
			Input.Color2B:Show()
			Input.Color2R:SetText(settings.color2[1])
			Input.Color2G:SetText(settings.color2[2])
			Input.Color2B:SetText(settings.color2[3])
		Label.Color3:Show()
			Input.Color3R:Show()
			Input.Color3G:Show()
			Input.Color3B:Show()
			Input.Color3R:SetText(settings.color3[1])
			Input.Color3G:SetText(settings.color3[2])
			Input.Color3B:SetText(settings.color3[3])
	elseif selected == "Morale" then
		Label.Color2:Show()
			Input.Color2R:Show()
			Input.Color2G:Show()
			Input.Color2B:Show()
			Input.Color2R:SetText(settings.color2[1])
			Input.Color2G:SetText(settings.color2[2])
			Input.Color2B:SetText(settings.color2[3])

		elseif selected == "Friend HP" or selected == "Enemy HP" then
			Label.UseLTI:Show()
			Input.UseLTI:Show()
			Input.UseLTI:SetValue(settings.uselti)
		else
			Label.Color2:Hide()
			Label.UseLTI:Hide()
			Input.UseLTI:Hide()
			Input.Color2R:Hide()
			Input.Color2G:Hide()
			Input.Color2B:Hide()
		Label.Color3:Hide()
			Input.Color3R:Hide()
			Input.Color3G:Hide()
			Input.Color3B:Hide()
		end
	elseif BUTTONS[activePanel] == "Texture" then
		Input.Texture:Select(settings.texture or "none")
	elseif BUTTONS[activePanel] == "Alpha" then
		Input.Alpha:SetText(settings.alpha)
		Input.BgAlpha:SetText(settings.bgalpha)
		Input.OocAlpha:SetText(settings.ooc_alpha)
		Input.NoTargetFade:SetValue(settings.no_target_fade)
	elseif BUTTONS[activePanel] == "Filling" then
		Input.Direction:Select(settings.direction)
		Input.Smooth:SetValue(settings.smooth)
		Input.Inverse:SetValue(settings.inverse)
	elseif BUTTONS[activePanel] == "Behaviour" then
		Input.ClickThrough:SetValue(settings.clickthrough)
	elseif BUTTONS[activePanel] == "Texts" then
		Input.Text:SetText(settings.text)
		Input.ColorR:SetText(settings.color[1])
		Input.ColorG:SetText(settings.color[2])
		Input.ColorB:SetText(settings.color[3])
		Input.Font:Select(settings.font)
		Input.Align:Select(settings.align)
		if settings.parent == "Root" then
			Input.Parent:Select("show always")
		else
			for k,v in pairs(BARLIST) do
				if v == settings.parent then
					Input.Parent:Select(k)
				end
			end
		end
	elseif BUTTONS[activePanel] == "Buffs" then
		Input.Enabled:SetValue(settings.enabled)
		Input.Size:SetText(settings.size)
		Input.Direction1:Select(settings.direction1)
		Input.Direction2:Select(settings.direction2)
		Input.Max:SetText(settings.max)
		Input.Arc:SetText(settings.arc)
		Input.Alpha:SetText(settings.alpha)
		Input.OocAlpha:SetText(settings.ooc_alpha)
		Input.NoTargetFade:SetValue(settings.no_target_fade)
		Input.ClickThrough:SetValue(settings.clickthrough)
		Input.SelfBuff:SetValue(settings.selfbuff)
	elseif BUTTONS[activePanel] == "Positioning" then
		Input.LayoutEditor:SetValue(settings.layouteditor)
		Input.X:SetText(settings.x)
		Input.Y:SetText(settings.y)
	elseif BUTTONS[activePanel] == "Profiles" then
		if selected then
			Input.Name:SetText(selected)
		end
	end

end

function xHUD.ApplyButtonPressed()
	local Input = Panel.Containers[activePanel].Input
	local selected = S(Input.Frame:Selected())
	local settings = {}

	if BUTTONS[activePanel] == "General" then
		if selected ~= "apply to all" then
			if ICONLIST[selected] then
				settings = xHUD.Settings[ICONLIST[selected]]
			else
				settings = xHUD.Settings[BARLIST[selected]]
			end
		end
	elseif BUTTONS[activePanel] == "Texts" then
		settings = xHUD.Settings.Texts[selected]
	elseif BUTTONS[activePanel] == "Buffs" then
		if selected ~= "apply to all" then
			settings = xHUD.Settings[BUFFLIST[selected]]
		end
	elseif BUTTONS[activePanel] == "Positioning" then
		if selected ~= "apply to all" then
			if BARLIST[selected] then
				settings = xHUD.Settings[BARLIST[selected]]
			elseif BUFFLIST[selected] then
				settings = xHUD.Settings[BUFFLIST[selected]]
			elseif ICONLIST[selected] then
				settings = xHUD.Settings[ICONLIST[selected]]
			else
				settings = xHUD.Settings.Texts[selected:match("(.+) %[Text%]")]
			end
		end
	elseif BUTTONS[activePanel] == "Icon Scale" then
		settings = xHUD.Settings[ICONLIST[selected]]
	elseif BUTTONS[activePanel] == "Profiles" then
		return
	else
		if selected ~= "apply to all" then
			settings = xHUD.Settings[BARLIST[selected]]
		end
	end

	if BUTTONS[activePanel] == "General" then
		settings.enabled = Input.Enabled:GetValue()
	elseif BUTTONS[activePanel] == "Size" then
		settings.width = tonumber(S(Input.Width:GetText()))
		settings.height = tonumber(S(Input.Height:GetText()))
		settings.padding = tonumber(S(Input.Padding:GetText()))
	elseif BUTTONS[activePanel] == "Icon Scale" then
		settings.scale = tonumber(S(Input.Scale:GetText()))
	elseif BUTTONS[activePanel] == "Color" then
		settings.color = {tonumber(S(Input.ColorR:GetText())),
							tonumber(S(Input.ColorG:GetText())),
							tonumber(S(Input.ColorB:GetText()))}
		settings.bgcolor = {tonumber(S(Input.BgColorR:GetText())),
							tonumber(S(Input.BgColorG:GetText())),
							tonumber(S(Input.BgColorB:GetText()))}
		if selected == "Career" then
			settings.color2 = {tonumber(S(Input.Color2R:GetText())),
								tonumber(S(Input.Color2G:GetText())),
								tonumber(S(Input.Color2B:GetText()))}
		settings.color3 = {tonumber(S(Input.Color3R:GetText())),
							tonumber(S(Input.Color3G:GetText())),
							tonumber(S(Input.Color3B:GetText()))}
	elseif selected == "Morale" then
		settings.color = {tonumber(S(Input.Color2R:GetText())),
							tonumber(S(Input.Color2G:GetText())),
							tonumber(S(Input.Color2B:GetText()))}
		elseif selected == "Friend HP" or selected == "Enemy HP" then
			settings.uselti = Input.UseLTI:GetValue()
		end
	elseif BUTTONS[activePanel] == "Texture" then
		settings.texture = S(Input.Texture:Selected())
	elseif BUTTONS[activePanel] == "Alpha" then
		settings.alpha = tonumber(S(Input.Alpha:GetText()))
		settings.bgalpha = tonumber(S(Input.BgAlpha:GetText()))
		settings.ooc_alpha = tonumber(S(Input.OocAlpha:GetText()))
		settings.no_target_fade = Input.NoTargetFade:GetValue()
	elseif BUTTONS[activePanel] == "Filling" then
		settings.direction = S(Input.Direction:Selected())
		settings.smooth = Input.Smooth:GetValue()
		settings.inverse = Input.Inverse:GetValue()
	elseif BUTTONS[activePanel] == "Behaviour" then
		settings.clickthrough = Input.ClickThrough:GetValue()
	elseif BUTTONS[activePanel] == "Texts" then
		settings.text = S(Input.Text:GetText())
		settings.color = {tonumber(S(Input.ColorR:GetText())),
							tonumber(S(Input.ColorG:GetText())),
							tonumber(S(Input.ColorB:GetText()))}
		settings.align = S(Input.Align:Selected())
		settings.font = S(Input.Font:Selected())
		settings.parent = BARLIST[S(Input.Parent:Selected())]
		if not settings.parent then
			settings.parent = "Root"
		end
	elseif BUTTONS[activePanel] == "Buffs" then
		settings.enabled = Input.Enabled:GetValue()
		settings.size = tonumber(S(Input.Size:GetText()))
		settings.direction1 = S(Input.Direction1:Selected())
		settings.direction2 = S(Input.Direction2:Selected())
		settings.max = tonumber(S(Input.Max:GetText()))
		settings.arc = tonumber(S(Input.Arc:GetText()))
		settings.alpha = tonumber(S(Input.Alpha:GetText()))
		settings.ooc_alpha = tonumber(S(Input.OocAlpha:GetText()))
		settings.no_target_fade = Input.NoTargetFade:GetValue()
		settings.clickthrough = Input.ClickThrough:GetValue()
		settings.selfbuff = Input.SelfBuff:GetValue()
	elseif BUTTONS[activePanel] == "Positioning" then
		settings.layouteditor = Input.LayoutEditor:GetValue()
		settings.x = tonumber(S(Input.X:GetText()))
		settings.y = tonumber(S(Input.Y:GetText()))
	end

	if selected == "apply to all" then
		if BUTTONS[activePanel] == "Buffs" then
			for i,val in ipairs(xHUD.BuffList) do
				for k,v in pairs(settings) do
					xHUD.Settings[val][k] = v
				end
			end
		elseif BUTTONS[activePanel] == "Positioning" then
			for i,val in ipairs(AllSettings()) do
				val.layouteditor = settings.layouteditor
			end
		else
			for i,val in ipairs(xHUD.BarList) do
				for k,v in pairs(settings) do
					xHUD.Settings[val][k] = v
				end
			end
		end
	end

	xHUD.Recreate()
end

function xHUD.PositioningButtonPressed(dirPushed)
	direction = dirPushed
end

function xHUD.PosButtonPressed()
	if direction == nil then return end

	local Input = Panel.Containers[activePanel].Input
	local selected = S(Input.Frame:Selected())
	local settings = {}

	if selected == "apply to all" then
		for i,val in ipairs(AllSettings()) do
			if direction == "up" then
				val.y = val.y-2
			elseif direction == "down" then
				val.y = val.y+2
			elseif direction == "left" then
				val.x = val.x-2
			elseif direction == "right" then
				val.x = val.x+2
			end
		end
	else
		if BARLIST[selected] then
			settings = xHUD.Settings[BARLIST[selected]]
		elseif BUFFLIST[selected] then
			settings = xHUD.Settings[BUFFLIST[selected]]
		elseif ICONLIST[selected] then
			settings = xHUD.Settings[ICONLIST[selected]]
		else
			settings = xHUD.Settings.Texts[selected:match("(.+) %[Text%]")]
		end
		
		if direction == "up" then
			settings.y = settings.y-2
		elseif direction == "down" then
			settings.y = settings.y+2
		elseif direction == "left" then
			settings.x = settings.x-2
		elseif direction == "right" then
			settings.x = settings.x+2
		end
	end
	xHUD.Recreate()
	selectedIndex = nil
end


function xHUD.ClearDirection()
	direction = nil
end


function xHUD.CreateGUI()
	Menu = LibGUI("Blackframe")
	Menu:MakeMovable()
	Menu:Resize(200,80+40*#BUTTONS)
	Menu:AnchorTo("Root","center","center",0,0)

	Menu.CloseButton = Menu("Closebutton")
	Menu.CloseButton.OnLButtonUp = function() Menu:Hide() end

	Menu.Title = Menu("Label")
	Menu.Title:Resize(200)
	Menu.Title:AnchorTo(Menu, "top", "top", 0, 10)
	Menu.Title:Font("font_clear_large_bold")
	Menu.Title:SetText("xHUD Settings")
	Menu.Title:Align("center")
	Menu.Title:IgnoreInput()
 
	 for i,val in ipairs(BUTTONS) do
		 local B = Menu("Button")
		 B:Resize(165)
		 B:SetText(val)
		 B:AnchorTo(Menu,"top","top",0,20+40*i)
		 B.OnLButtonUp = function() xHUD.MenuButtonPressed(i) end
	end

	Menu:Hide()

	Panel = LibGUI("Blackframe")
	Panel:MakeMovable()
	Panel:Resize(500,600)
	Panel:AnchorTo("Root","right","right",0,0)

	Panel.CloseButton = Panel("Closebutton")
	Panel.CloseButton.OnLButtonUp = function() Panel:Hide() end

	Panel.Title = Panel("Label")
	Panel.Title:Resize(250)
	Panel.Title:AnchorTo(Panel, "top", "top", 0, 10)
	Panel.Title:Font("font_clear_large_bold")
	Panel.Title:Align("center")
	Panel.Title:IgnoreInput()

	Panel.Containers = {}

	Panel.ApplyButton = Panel("Button")
	Panel.ApplyButton:SetText("Apply")
	Panel.ApplyButton:Resize(200)
	Panel.ApplyButton:AnchorTo(Panel,"bottomleft","bottomleft",20,-20)
	Panel.ApplyButton.OnLButtonUp = function() xHUD.ApplyButtonPressed() end

	Panel.CancelButton = Panel("Button")
	Panel.CancelButton:SetText("Cancel")
	Panel.CancelButton:Resize(200)
	Panel.CancelButton:AnchorTo(Panel,"bottomright","bottomright",-20,-20)
	Panel.CancelButton.OnLButtonUp = function() Panel:Hide() end

	Panel:Hide()

	Vars = LibGUI("Blackframe")
	Vars:MakeMovable()
	Vars:Resize(400,500)
	Vars:AnchorTo(Panel,"topleft","topright",0,0)
	Vars:Hide()

	Vars.CloseButton = Vars("Closebutton")
	Vars.CloseButton.OnLButtonUp = function() Vars:Hide() end

	Vars.Title = Vars("Label")
	Vars.Title:SetText("Text Variables")
	Vars.Title:Resize(250)
	Vars.Title:AnchorTo(Vars, "top", "top", 0, 10)
	Vars.Title:Font("font_clear_large_bold")
	Vars.Title:Align("center")
	Vars.Title:IgnoreInput()

	local lines = {
		"$cur_hp",
		"$max_hp",
		"$percent_hp",
		"$diff_hp",
		"$cur_ap",
		"$max_ap",
		"$percent_ap",
		"$cur_career",
		"$max_career",
		"$percent_career",
		"$cur_morale",
		"$max_morale",
		"$morale_level",
		"$percent_morale",
		"$cur_xp",
		"$needed_xp",
		"$rested_xp",
		"$renown_rank",
		"$cur_rp",
		"$needed_rp",
		"$cur_inf",
		"$needed_inf",
		"$step_inf",
		"$cur_casttime",
		"$max_casttime",
		"$pushback_casttime",
		"$spell",
		"$friend_hp",
		"$enemy_hp",
		"$player_name",
		"$friend_name",
		"$enemy_name",
		"$player_level",
		"$friend_level",
		"$enemy_level",
		"$friend_tier",
		"$enemy_tier",
		}

	for i,k in ipairs(lines) do
		Vars["Line"..i] = Vars("Label")
		Vars["Line"..i]:SetText(k)
		if i%2 == 0 then
			Vars["Line"..i]:Position(220,30+i*10)
		else
			Vars["Line"..i]:Position(20,40+i*10)
		end
		Vars["Line"..i]:Align("left")
	end
end

function xHUD.CreateContainer(index)
	local C = Panel("Window")
	Panel.Containers[index] = C

	C:Position(0,60)

	C.Label = {}
	C.Input = {}

	if BUTTONS[index] == "General" then
		C.Label.Delay = C("Label")
		C.Label.Delay:SetText("general update delay")
		C.Label.Delay:Align("right")
		C.Label.Delay:Resize(200)
		C.Label.Delay:Position(40,0)
		C.Input.Delay = C("Textbox")
		C.Input.Delay:SetText(xHUD.Settings.updateDelay)
		C.Input.Delay:Resize(100)
		C.Input.Delay:AnchorTo(C.Label.Delay,"right","left",5,-5)

		C.Input.Reset = C("Button")
		C.Input.Reset:SetText("Reset everything to Default")
		C.Input.Reset:Resize(260)
		C.Input.Reset:Position(20,120)
		C.Input.Reset.OnLButtonUp = function() C.Input.Reset:Disable() C.Input.AcceptReset:Show() end

		C.Input.AcceptReset = C("Button")
		C.Input.AcceptReset:SetText("Accept")
		C.Input.AcceptReset:Resize(180)
		C.Input.AcceptReset:Position(300,120)
		C.Input.AcceptReset:Hide()
		C.Input.AcceptReset.OnLButtonUp = function() C.Input.Reset:Enable() C.Input.AcceptReset:Hide() xHUD.RevertToDefault() end

		C.Label.Frame = C("Label")
		C.Label.Frame:SetText("apply to frame:")
		C.Label.Frame:Align("right")
		C.Label.Frame:Resize(200)
		C.Label.Frame:Position(0,160)
		C.Input.Frame = C("Combobox")
		C.Input.Frame:AnchorTo(C.Label.Frame,"right","left",5,-5)
		C.Input.Frame:Add("apply to all")
		for k,v in pairs(BARLIST) do
			C.Input.Frame:Add(k)
		end
		for k,v in pairs(ICONLIST) do
			C.Input.Frame:Add(k)
		end
		C.Input.Frame:SelectIndex(1)

		C.Label.Enabled = C("Label")
		C.Label.Enabled:SetText("enabled")
		C.Label.Enabled:Align("right")
		C.Label.Enabled:Resize(200)
		C.Label.Enabled:Position(0,200)
		C.Input.Enabled = C("Checkbox")
		C.Input.Enabled:AnchorTo(C.Label.Enabled,"right","left",5,-5)

		C:Resize(500,240)

	elseif BUTTONS[index] == "Size" then
		C.Label.Frame = C("Label")
		C.Label.Frame:SetText("apply to frame:")
		C.Label.Frame:Align("right")
		C.Label.Frame:Resize(200)
		C.Label.Frame:Position(0,0)
		C.Input.Frame = C("Combobox")
		C.Input.Frame:AnchorTo(C.Label.Frame,"right","left",5,-5)
		C.Input.Frame:Add("apply to all")
		for k,v in pairs(BARLIST) do
			C.Input.Frame:Add(k)
		end
		C.Input.Frame:SelectIndex(1)

		C.Label.Width = C("Label")
		C.Label.Width:SetText("width")
		C.Label.Width:Align("right")
		C.Label.Width:Resize(200)
		C.Label.Width:Position(0,40)
		C.Input.Width = C("Textbox")
		C.Input.Width:Resize(200)
		C.Input.Width:AnchorTo(C.Label.Width,"right","left",5,-5)

		C.Label.Height = C("Label")
		C.Label.Height:SetText("height")
		C.Label.Height:Align("right")
		C.Label.Height:Resize(200)
		C.Label.Height:Position(0,80)
		C.Input.Height = C("Textbox")
		C.Input.Height:Resize(200)
		C.Input.Height:AnchorTo(C.Label.Height,"right","left",5,-5)

		C.Label.Padding = C("Label")
		C.Label.Padding:SetText("padding")
		C.Label.Padding:Align("right")
		C.Label.Padding:Resize(200)
		C.Label.Padding:Position(0,120)
		C.Input.Padding = C("Textbox")
		C.Input.Padding:Resize(200)
		C.Input.Padding:AnchorTo(C.Label.Padding,"right","left",5,-5)

		C:Resize(500,160)

elseif BUTTONS[index] == "Icon Scale" then
		C.Label.Frame = C("Label")
		C.Label.Frame:SetText("apply to frame:")
		C.Label.Frame:Align("right")
		C.Label.Frame:Resize(200)
		C.Label.Frame:Position(0,0)
		C.Input.Frame = C("Combobox")
		C.Input.Frame:AnchorTo(C.Label.Frame,"right","left",5,-5)
		C.Input.Frame:Add("apply to all")
		for k,v in pairs(ICONLIST) do
			C.Input.Frame:Add(k)
		end
		C.Input.Frame:SelectIndex(1)

		C.Label.Scale = C("Label")
		C.Label.Scale :SetText("scale")
		C.Label.Scale :Align("right")
		C.Label.Scale :Resize(200)
		C.Label.Scale :Position(0,40)
		C.Input.Scale = C("Textbox")
		C.Input.Scale :Resize(200)
		C.Input.Scale :AnchorTo(C.Label.Scale ,"right","left",5,-5)

		C:Resize(500,160)

	elseif BUTTONS[index] == "Color" then
		C.Label.Frame = C("Label")
		C.Label.Frame:SetText("apply to frame:")
		C.Label.Frame:Align("right")
		C.Label.Frame:Resize(200)
		C.Label.Frame:Position(0,0)
		C.Input.Frame = C("Combobox")
		C.Input.Frame:AnchorTo(C.Label.Frame,"right","left",5,-5)
		C.Input.Frame:Add("apply to all")
		for k,v in pairs(BARLIST) do
			C.Input.Frame:Add(k)
		end
		C.Input.Frame:SelectIndex(1)

		C.Label.Color = C("Label")
		C.Label.Color:SetText("Main color")
		C.Label.Color:Align("right")
		C.Label.Color:Resize(200)
		C.Label.Color:Position(0,40)
		C.Input.ColorR = C("Textbox")
		C.Input.ColorR:Resize(80)
		C.Input.ColorR:AnchorTo(C.Label.Color,"right","left",5,-5)
		C.Input.ColorG = C("Textbox")
		C.Input.ColorG:Resize(80)
		C.Input.ColorG:AnchorTo(C.Input.ColorR,"right","left",5,0)
		C.Input.ColorB = C("Textbox")
		C.Input.ColorB:Resize(80)
		C.Input.ColorB:AnchorTo(C.Input.ColorG,"right","left",5,0)

		C.Label.BgColor = C("Label")
		C.Label.BgColor:SetText("Background color")
		C.Label.BgColor:Align("right")
		C.Label.BgColor:Resize(200)
		C.Label.BgColor:Position(0,80)
		C.Input.BgColorR = C("Textbox")
		C.Input.BgColorR:Resize(80)
		C.Input.BgColorR:AnchorTo(C.Label.BgColor,"right","left",5,-5)
		C.Input.BgColorG = C("Textbox")
		C.Input.BgColorG:Resize(80)
		C.Input.BgColorG:AnchorTo(C.Input.BgColorR,"right","left",5,0)
		C.Input.BgColorB = C("Textbox")
		C.Input.BgColorB:Resize(80)
		C.Input.BgColorB:AnchorTo(C.Input.BgColorG,"right","left",5,0)

		-- Career and Morale
		C.Label.Color2 = C("Label")
		C.Label.Color2:SetText("Secondary color")
		C.Label.Color2:Align("right")
		C.Label.Color2:Resize(200)
		C.Label.Color2:Position(0,120)
		C.Input.Color2R = C("Textbox")
		C.Input.Color2R:Resize(80)
		C.Input.Color2R:AnchorTo(C.Label.Color2,"right","left",5,-5)
		C.Input.Color2G = C("Textbox")
		C.Input.Color2G:Resize(80)
		C.Input.Color2G:AnchorTo(C.Input.Color2R,"right","left",5,0)
		C.Input.Color2B = C("Textbox")
		C.Input.Color2B:Resize(80)
		C.Input.Color2B:AnchorTo(C.Input.Color2G,"right","left",5,0)
		C.Label.Color2:Hide()
		C.Input.Color2R:Hide()
		C.Input.Color2G:Hide()
		C.Input.Color2B:Hide()
		C.Label.Color3 = C("Label")
		C.Label.Color3:SetText("Full color")
		C.Label.Color3:Align("right")
		C.Label.Color3:Resize(200)
		C.Label.Color3:Position(0,160)
		C.Input.Color3R = C("Textbox")
		C.Input.Color3R:Resize(80)
		C.Input.Color3R:AnchorTo(C.Label.Color3,"right","left",5,-5)
		C.Input.Color3G = C("Textbox")
		C.Input.Color3G:Resize(80)
		C.Input.Color3G:AnchorTo(C.Input.Color3R,"right","left",5,0)
		C.Input.Color3B = C("Textbox")
		C.Input.Color3B:Resize(80)
		C.Input.Color3B:AnchorTo(C.Input.Color3G,"right","left",5,0)
		C.Label.Color3:Hide()
		C.Input.Color3R:Hide()
		C.Input.Color3G:Hide()
		C.Input.Color3B:Hide()

		-- Friendly and Hostile Target
		C.Label.UseLTI = C("Label")
		C.Label.UseLTI:SetText("use LTI colors")
		C.Label.UseLTI:Align("right")
		C.Label.UseLTI:Resize(200)
		C.Label.UseLTI:Position(0,120)
		C.Input.UseLTI = C("Checkbox")
		C.Input.UseLTI:AnchorTo(C.Label.UseLTI,"right","left",5,-5)
		C.Label.UseLTI:Hide()
		C.Input.UseLTI:Hide()

		C:Resize(500,200)

	elseif BUTTONS[index] == "Texture" then
		C.Label.Frame = C("Label")
		C.Label.Frame:SetText("apply to frame:")
		C.Label.Frame:Align("right")
		C.Label.Frame:Resize(200)
		C.Label.Frame:Position(0,0)
		C.Input.Frame = C("Combobox")
		C.Input.Frame:AnchorTo(C.Label.Frame,"right","left",5,-5)
		C.Input.Frame:Add("apply to all")
		for k,v in pairs(BARLIST) do
			C.Input.Frame:Add(k)
		end
		C.Input.Frame:SelectIndex(1)

		C.Label.Texture = C("Label")
		C.Label.Texture:SetText("texture")
		C.Label.Texture:Align("right")
		C.Label.Texture:Resize(200)
		C.Label.Texture:Position(0,40)
		C.Input.Texture = C("Combobox")
		C.Input.Texture:AnchorTo(C.Label.Texture,"right","left",5,-5)
		C.Input.Texture:Add("none")
		for k,v in pairs(xHUD.Textures) do
			C.Input.Texture:Add(k)
		end

		C:Resize(500,80)

	elseif BUTTONS[index] == "Alpha" then
		C.Label.Frame = C("Label")
		C.Label.Frame:SetText("apply to frame:")
		C.Label.Frame:Align("right")
		C.Label.Frame:Resize(200)
		C.Label.Frame:Position(0,0)
		C.Input.Frame = C("Combobox")
		C.Input.Frame:AnchorTo(C.Label.Frame,"right","left",5,-5)
		C.Input.Frame:Add("apply to all")
		for k,v in pairs(BARLIST) do
			C.Input.Frame:Add(k)
		end
		C.Input.Frame:SelectIndex(1)

		C.Label.Alpha = C("Label")
		C.Label.Alpha:SetText("alpha")
		C.Label.Alpha:Align("right")
		C.Label.Alpha:Resize(200)
		C.Label.Alpha:Position(0,40)
		C.Input.Alpha = C("Textbox")
		C.Input.Alpha:Resize(200)
		C.Input.Alpha:AnchorTo(C.Label.Alpha,"right","left",5,-5)

		C.Label.BgAlpha = C("Label")
		C.Label.BgAlpha:SetText("background alpha")
		C.Label.BgAlpha:Align("right")
		C.Label.BgAlpha:Resize(200)
		C.Label.BgAlpha:Position(0,80)
		C.Input.BgAlpha = C("Textbox")
		C.Input.BgAlpha:Resize(200)
		C.Input.BgAlpha:AnchorTo(C.Label.BgAlpha,"right","left",5,-5)

		C.Label.OocAlpha = C("Label")
		C.Label.OocAlpha:SetText("out of combat alpha")
		C.Label.OocAlpha:Align("right")
		C.Label.OocAlpha:Resize(200)
		C.Label.OocAlpha:Position(0,120)
		C.Input.OocAlpha = C("Textbox")
		C.Input.OocAlpha:Resize(200)
		C.Input.OocAlpha:AnchorTo(C.Label.OocAlpha,"right","left",5,-5)

		C.Label.NoTargetFade = C("Label")
		C.Label.NoTargetFade:Align("right")
		C.Label.NoTargetFade:Resize(220)
		C.Label.NoTargetFade:SetText("don't fade with a target")
		C.Label.NoTargetFade:Position(0,160)
		C.Input.NoTargetFade = C("Checkbox")
		C.Input.NoTargetFade:AnchorTo(C.Label.NoTargetFade,"right","left",5,-5)

		C:Resize(500,200)

	elseif BUTTONS[index] == "Filling" then
		C.Label.Frame = C("Label")
		C.Label.Frame:SetText("apply to frame:")
		C.Label.Frame:Align("right")
		C.Label.Frame:Resize(200)
		C.Label.Frame:Position(0,0)
		C.Input.Frame = C("Combobox")
		C.Input.Frame:AnchorTo(C.Label.Frame,"right","left",5,-5)
		C.Input.Frame:Add("apply to all")
		for k,v in pairs(BARLIST) do
			C.Input.Frame:Add(k)
		end
		C.Input.Frame:SelectIndex(1)

		C.Label.Direction = C("Label")
		C.Label.Direction:SetText("fill direction")
		C.Label.Direction:Align("right")
		C.Label.Direction:Resize(200)
		C.Label.Direction:Position(0,40)
		C.Input.Direction = C("Combobox")
		C.Input.Direction:AnchorTo(C.Label.Direction,"right","left",5,-5)
		C.Input.Direction:Add("up")
		C.Input.Direction:Add("down")
		C.Input.Direction:Add("left")
		C.Input.Direction:Add("right")

		C.Label.Smooth = C("Label")
		C.Label.Smooth:SetText("smooth fading")
		C.Label.Smooth:Align("right")
		C.Label.Smooth:Resize(200)
		C.Label.Smooth:Position(0,80)
		C.Input.Smooth = C("Checkbox")
		C.Input.Smooth:AnchorTo(C.Label.Smooth,"right","left",5,-5)

		C.Label.Inverse = C("Label")
		C.Label.Inverse:SetText("inverse filling")
		C.Label.Inverse:Align("right")
		C.Label.Inverse:Resize(200)
		C.Label.Inverse:Position(0,120)
		C.Input.Inverse = C("Checkbox")
		C.Input.Inverse:AnchorTo(C.Label.Inverse,"right","left",5,-5)
		-- don't use yet
		C.Label.Inverse:Hide()
		C.Input.Inverse:Hide()

		C:Resize(500,160)

	elseif BUTTONS[index] == "Behaviour" then
		C.Label.Frame = C("Label")
		C.Label.Frame:SetText("apply to frame:")
		C.Label.Frame:Align("right")
		C.Label.Frame:Resize(200)
		C.Label.Frame:Position(0,0)
		C.Input.Frame = C("Combobox")
		C.Input.Frame:AnchorTo(C.Label.Frame,"right","left",5,-5)
		C.Input.Frame:Add("apply to all")
		for k,v in pairs(BARLIST) do
			C.Input.Frame:Add(k)
		end
		C.Input.Frame:SelectIndex(1)

		C.Label.ClickThrough = C("Label")
		C.Label.ClickThrough:SetText("click through frame")
		C.Label.ClickThrough:Align("right")
		C.Label.ClickThrough:Resize(200)
		C.Label.ClickThrough:Position(0,40)
		C.Input.ClickThrough = C("Checkbox")
		C.Input.ClickThrough:AnchorTo(C.Label.ClickThrough,"right","left",5,-5)

		C:Resize(500,80)

	elseif BUTTONS[index] == "Texts" then
		C.Label.Create = C("Label")
		C.Label.Create:SetText("create new text:")
		C.Label.Create:Align("right")
		C.Label.Create:Resize(200)
		C.Label.Create:Position(0,0)
		C.Input.Create = C("Textbox")
		C.Input.Create:Resize(180)
		C.Input.Create:AnchorTo(C.Label.Create,"right","left",5,-5)
		C.Input.CreateButton = C("Button")
		C.Input.CreateButton:SetText("create")
		C.Input.CreateButton:Resize(80)
		C.Input.CreateButton:AnchorTo(C.Input.Create,"right","left",5,0)
		C.Input.CreateButton.OnLButtonUp = 
			function() 
				local name = S(C.Input.Create:GetText()) 
				C.Input.Create:SetText("")
				xHUD.CreateNewTextPanel(name) 
				C.Input.Frame:Add(name) selectedIndex = nil
				C.Input.Frame:Select(name)
			end

		C.Label.Frame = C("Label")
		C.Label.Frame:SetText("or choose one:")
		C.Label.Frame:Align("right")
		C.Label.Frame:Resize(200)
		C.Label.Frame:Position(0,40)
		C.Input.Frame = C("Combobox")
		C.Input.Frame:AnchorTo(C.Label.Frame,"right","left",5,-5)
		for k,v in pairs(xHUD.Settings.Texts) do
			C.Input.Frame:Add(k)
		end
		C.Input.Frame:SelectIndex(1)

		C.Label.Text = C("Label")
		C.Label.Text:SetText("text")
		C.Label.Text:Align("right")
		C.Label.Text:Resize(200)
		C.Label.Text:Position(0,80)
		C.Input.Text = C("Textbox")
		C.Input.Text:Resize(200)
		C.Input.Text:AnchorTo(C.Label.Text,"right","left",5,-5)

		C.Input.Vars = C("Button")
		C.Input.Vars:SetText("Variables")
		C.Input.Vars:Resize(100)
		C.Input.Vars:Position(0,60)
		C.Input.Vars.OnLButtonUp = function()  if Vars:Showing() then Vars:Hide() else Vars:Show() end  end

		C.Label.Parent = C("Label")
		C.Label.Parent:SetText("show with")
		C.Label.Parent:Align("right")
		C.Label.Parent:Resize(200)
		C.Label.Parent:Position(0,120)
		C.Input.Parent = C("Combobox")
		C.Input.Parent:AnchorTo(C.Label.Parent,"right","left",5,-5)
		C.Input.Parent:Add("show always")
		for k,v in pairs(BARLIST) do
			C.Input.Parent:Add(k)
		end

		C.Label.Align = C("Label")
		C.Label.Align:SetText("align")
		C.Label.Align:Align("right")
		C.Label.Align:Resize(200)
		C.Label.Align:Position(0,160)
		C.Input.Align = C("Combobox")
		C.Input.Align:AnchorTo(C.Label.Align,"right","left",5,-5)
		C.Input.Align:Add("left")
		C.Input.Align:Add("right")
		C.Input.Align:Add("center")

		C.Label.Font = C("Label")
		C.Label.Font:SetText("font")
		C.Label.Font:Align("right")
		C.Label.Font:Resize(200)
		C.Label.Font:Position(0,200)
		C.Input.Font = C("Combobox")
		C.Input.Font:AnchorTo(C.Label.Font,"right","left",5,-5)
		C.Input.Font:Add("font_chat_text")
		C.Input.Font:Add("font_clear_tiny")
		C.Input.Font:Add("font_clear_small")
		C.Input.Font:Add("font_clear_medium")
		C.Input.Font:Add("font_clear_large")
		C.Input.Font:Add("font_clear_small_bold")
		C.Input.Font:Add("font_clear_medium_bold")
		C.Input.Font:Add("font_clear_large_bold")
		C.Input.Font:Add("font_default_text_small")
		C.Input.Font:Add("font_default_text")
		C.Input.Font:Add("font_default_text_large")
		C.Input.Font:Add("font_default_text_huge")
		C.Input.Font:Add("font_heading_small")
		C.Input.Font:Add("font_heading_large")
		C.Input.Font:Add("font_heading_huge")
		C.Input.Font:Add("font_journal_text_small")
		C.Input.Font:Add("font_journal_text")

		C.Label.Color = C("Label")
		C.Label.Color:SetText("color")
		C.Label.Color:Align("right")
		C.Label.Color:Resize(200)
		C.Label.Color:Position(0,240)
		C.Input.ColorR = C("Textbox")
		C.Input.ColorR:Resize(80)
		C.Input.ColorR:AnchorTo(C.Label.Color,"right","left",5,-5)
		C.Input.ColorG = C("Textbox")
		C.Input.ColorG:Resize(80)
		C.Input.ColorG:AnchorTo(C.Input.ColorR,"right","left",5,0)
		C.Input.ColorB = C("Textbox")
		C.Input.ColorB:Resize(80)
		C.Input.ColorB:AnchorTo(C.Input.ColorG,"right","left",5,0)

		C.Input.Delete = C("Button")
		C.Input.Delete:SetText("Delete this Panel")
		C.Input.Delete:Resize(250)
		C.Input.Delete:Position(200,280)
		C.Input.Delete.OnLButtonUp =
			function()
				local name = S(C.Input.Frame:Selected())
				LayoutEditor.UnregisterWindow("xHUDTexts_"..name)
				DestroyWindow("xHUDTexts_"..name)
				xHUD.Settings.Texts[name] = nil
				C.Input.Frame:Clear()
				for k,v in pairs(xHUD.Settings.Texts) do
					C.Input.Frame:Add(k)
				end
				selectedIndex = nil
				C.Input.Frame:SelectIndex(1)
			end
			
		C:Resize(500,320)

	elseif BUTTONS[index] == "Buffs" then
		C.Label.Frame = C("Label")
		C.Label.Frame:SetText("apply to frame:")
		C.Label.Frame:Align("right")
		C.Label.Frame:Resize(200)
		C.Label.Frame:Position(0,0)
		C.Input.Frame = C("Combobox")
		C.Input.Frame:AnchorTo(C.Label.Frame,"right","left",5,-5)
		C.Input.Frame:Add("apply to all")
		for k,v in pairs(BUFFLIST) do
			C.Input.Frame:Add(k)
		end
		C.Input.Frame:SelectIndex(1)

		C.Label.Enabled = C("Label")
		C.Label.Enabled:SetText("enabled")
		C.Label.Enabled:Align("right")
		C.Label.Enabled:Resize(200)
		C.Label.Enabled:Position(0,40)
		C.Input.Enabled = C("Checkbox")
		C.Input.Enabled:AnchorTo(C.Label.Enabled,"right","left",5,-5)

		C.Label.Size = C("Label")
		C.Label.Size:SetText("size")
		C.Label.Size:Align("right")
		C.Label.Size:Resize(200)
		C.Label.Size:Position(0,80)
		C.Input.Size = C("Textbox")
		C.Input.Size:Resize(200)
		C.Input.Size:AnchorTo(C.Label.Size,"right","left",5,-5)

		C.Label.Direction1 = C("Label")
		C.Label.Direction1:SetText("1. growth direction")
		C.Label.Direction1:Align("right")
		C.Label.Direction1:Resize(200)
		C.Label.Direction1:Position(0,120)
		C.Input.Direction1 = C("Combobox")
		C.Input.Direction1:AnchorTo(C.Label.Direction1,"right","left",5,-5)
		C.Input.Direction1:Add("up")
		C.Input.Direction1:Add("down")
		C.Input.Direction1:Add("left")
		C.Input.Direction1:Add("right")

		C.Label.Direction2 = C("Label")
		C.Label.Direction2:SetText("2. growth direction")
		C.Label.Direction2:Align("right")
		C.Label.Direction2:Resize(200)
		C.Label.Direction2:Position(0,160)
		C.Input.Direction2 = C("Combobox")
		C.Input.Direction2:AnchorTo(C.Label.Direction2,"right","left",5,-5)
		C.Input.Direction2:Add("up")
		C.Input.Direction2:Add("down")
		C.Input.Direction2:Add("left")
		C.Input.Direction2:Add("right")

		C.Label.Max = C("Label")
		C.Label.Max:SetText("max buffs per row/col")
		C.Label.Max:Align("right")
		C.Label.Max:Resize(200)
		C.Label.Max:Position(0,200)
		C.Input.Max = C("Textbox")
		C.Input.Max:Resize(200)
		C.Input.Max:AnchorTo(C.Label.Max,"right","left",5,-5)

		C.Label.Arc = C("Label")
		C.Label.Arc:SetText("deflection")
		C.Label.Arc:Align("right")
		C.Label.Arc:Resize(200)
		C.Label.Arc:Position(0,240)
		C.Input.Arc = C("Textbox")
		C.Input.Arc:Resize(200)
		C.Input.Arc:AnchorTo(C.Label.Arc,"right","left",5,-5)

		C.Label.ClickThrough = C("Label")
		C.Label.ClickThrough:SetText("click through frame")
		C.Label.ClickThrough:Align("right")
		C.Label.ClickThrough:Resize(200)
		C.Label.ClickThrough:Position(0,280)
		C.Input.ClickThrough = C("Checkbox")
		C.Input.ClickThrough:AnchorTo(C.Label.ClickThrough,"right","left",5,-5)

		C.Label.Alpha = C("Label")
		C.Label.Alpha:SetText("alpha")
		C.Label.Alpha:Align("right")
		C.Label.Alpha:Resize(200)
		C.Label.Alpha:Position(0,320)
		C.Input.Alpha = C("Textbox")
		C.Input.Alpha:Resize(200)
		C.Input.Alpha:AnchorTo(C.Label.Alpha,"right","left",5,-5)

		C.Label.OocAlpha = C("Label")
		C.Label.OocAlpha:SetText("out of combat alpha")
		C.Label.OocAlpha:Align("right")
		C.Label.OocAlpha:Resize(200)
		C.Label.OocAlpha:Position(0,360)
		C.Input.OocAlpha = C("Textbox")
		C.Input.OocAlpha:Resize(200)
		C.Input.OocAlpha:AnchorTo(C.Label.OocAlpha,"right","left",5,-5)

		C.Label.NoTargetFade = C("Label")
		C.Label.NoTargetFade:Align("right")
		C.Label.NoTargetFade:Resize(220)
		C.Label.NoTargetFade:SetText("don't fade with a target")
		C.Label.NoTargetFade:Position(0,400)
		C.Input.NoTargetFade = C("Checkbox")
		C.Input.NoTargetFade:AnchorTo(C.Label.NoTargetFade,"right","left",5,-5)

		C.Label.SelfBuff = C("Label")
		C.Label.SelfBuff:Align("right")
		C.Label.SelfBuff:Resize(220)
		C.Label.SelfBuff:SetText("show only self-inflicted")
		C.Label.SelfBuff:Position(0,440)
		C.Input.SelfBuff = C("Checkbox")
		C.Input.SelfBuff:AnchorTo(C.Label.SelfBuff,"right","left",5,-5)

		C:Resize(500,480)

	elseif BUTTONS[index] == "Positioning" then
		C.Label.Frame = C("Label")
		C.Label.Frame:SetText("select frame:")
		C.Label.Frame:Align("right")
		C.Label.Frame:Resize(200)
		C.Label.Frame:Position(0,0)
		C.Input.Frame = C("Combobox")
		C.Input.Frame:AnchorTo(C.Label.Frame,"right","left",5,-5)
		C.Input.Frame:Add("apply to all")
		for k,v in pairs(BARLIST) do
			C.Input.Frame:Add(k)
		end
		for k,v in pairs(ICONLIST) do
			C.Input.Frame:Add(k)
		end
		for k,v in pairs(BUFFLIST) do
			C.Input.Frame:Add(k)
		end
		for k,v in pairs(xHUD.Settings.Texts) do
			C.Input.Frame:Add(k.." [Text]")
		end
		C.Input.Frame:SelectIndex(1)

		C.Input.LayoutEditor = C("Checkbox")
		C.Input.LayoutEditor:Position(205,35)
		C.Label.LayoutEditor = C("Label")
		C.Label.LayoutEditor:Align("left")
		C.Label.LayoutEditor:Resize(200)
		C.Label.LayoutEditor:SetText("show in Layout Editor")
		C.Label.LayoutEditor:AnchorTo(C.Input.LayoutEditor,"right","left",5,5)

		C.Label.X = C("Label")
		C.Label.X:SetText("x")
		C.Label.X:Align("right")
		C.Label.X:Resize(40)
		C.Label.X:Position(160,80)
		C.Input.X = C("Textbox")
		C.Input.X:Resize(200)
		C.Input.X:AnchorTo(C.Label.X,"right","left",5,-5)

		C.Label.Y = C("Label")
		C.Label.Y:SetText("y")
		C.Label.Y:Align("right")
		C.Label.Y:Resize(40)
		C.Label.Y:Position(160,120)
		C.Input.Y = C("Textbox")
		C.Input.Y:Resize(200)
		C.Input.Y:AnchorTo(C.Label.Y,"right","left",5,-5)

		C.Input.PositionUp = C("Button")
		C.Input.PositionUp:SetText("^")
		C.Input.PositionUp:Resize(40)
		C.Input.PositionUp:Position(80,40)
		C.Input.PositionUp.OnLButtonDown = function() xHUD.PositioningButtonPressed("up") end
		C.Input.PositionUp.OnLButtonUp = function() xHUD.ClearDirection() end

		C.Input.PositionRight = C("Button")
		C.Input.PositionRight:SetText(">")
		C.Input.PositionRight:Resize(40)
		C.Input.PositionRight:Position(120,80)	
		C.Input.PositionRight.OnLButtonDown = function() xHUD.PositioningButtonPressed("right") end
		C.Input.PositionRight.OnLButtonUp = function() xHUD.ClearDirection() end

		C.Input.PositionDown = C("Button")
		C.Input.PositionDown:SetText("v")
		C.Input.PositionDown:Resize(40)
		C.Input.PositionDown:Position(80,120) 
		C.Input.PositionDown.OnLButtonDown = function() xHUD.PositioningButtonPressed("down") end
		C.Input.PositionDown.OnLButtonUp = function() xHUD.ClearDirection() end

		C.Input.PositionLeft = C("Button")
		C.Input.PositionLeft:SetText("<")
		C.Input.PositionLeft:Resize(40)
		C.Input.PositionLeft:Position(40,80) 
		C.Input.PositionLeft.OnLButtonDown = function() xHUD.PositioningButtonPressed("left") end
		C.Input.PositionLeft.OnLButtonUp = function() xHUD.ClearDirection() end

		C:Resize(500,160)

		elseif BUTTONS[index] == "Profiles" then
		C.Label.Frame = C("Label")
		C.Label.Frame:SetText("select profile:")
		C.Label.Frame:Align("right")
		C.Label.Frame:Resize(200)
		C.Label.Frame:Position(0,0)
		C.Input.Frame = C("Combobox")
		C.Input.Frame:AnchorTo(C.Label.Frame,"right","left",5,-5)
		for k,v in pairs(xHUD.Profiles) do
			C.Input.Frame:Add(k)
		end

		C.Label.Name = C("Label")
		C.Label.Name:SetText("profile name")
		C.Label.Name:Align("right")
		C.Label.Name:Resize(200)
		C.Label.Name:Position(0,40)
		C.Input.Name = C("Textbox")
		C.Input.Name:Resize(200)
		C.Input.Name:AnchorTo(C.Label.Name,"right","left",5,-5)		

		C.Input.Save = C("Button")
		C.Input.Save:SetText("save as")
		C.Input.Save:Resize(100)
		C.Input.Save:Position(100,80)
		C.Input.Save.OnLButtonDown =
			function()
				local name = S(C.Input.Name:GetText())
				if name ~= "" then
					xHUD.Profiles[name] = xHUD.deepcopy(xHUD.Settings)
					EA_ChatWindow.Print(towstring("xHUD: Profile saved as '"..name.."'."))
					C.Input.Frame:Clear()
					for k,v in pairs(xHUD.Profiles) do
						C.Input.Frame:Add(k)
					end
				end
			end

		C.Input.Load = C("Button")
		C.Input.Load:SetText("load")
		C.Input.Load:Resize(100)
		C.Input.Load:Position(200,80)
		C.Input.Load.OnLButtonDown =
			function()
				local name = S(C.Input.Name:GetText())
				if xHUD.Profiles[name] then
					xHUD.Settings = xHUD.deepcopy(xHUD.Profiles[name])
					EA_ChatWindow.Print(towstring("xHUD: Loaded profile '"..name.."'."))
					xHUD.Recreate()
				end
			end

		C.Input.Delete = C("Button")
		C.Input.Delete:SetText("delete")
		C.Input.Delete:Resize(100)
		C.Input.Delete:Position(300,80)
		C.Input.Delete.OnLButtonDown =
			function()
				local name = S(C.Input.Name:GetText())
				if xHUD.Profiles[name] then
					xHUD.Profiles[name] = nil
					EA_ChatWindow.Print(towstring("xHUD: Deleted profile '"..name.."'."))
					C.Input.Frame:Clear()
					for k,v in pairs(xHUD.Profiles) do
						C.Input.Frame:Add(k)
					end
				end
			end

		C:Resize(500,120)

	end
end

function xHUD.OnUpdateGUI(elapsed)
	if Panel and Panel:Showing() then
		local si = Panel.Containers[activePanel].Input.Frame:SelectedIndex()
		if si ~= selectedIndex then
			selectedIndex = si
			xHUD.OnSelChangedFrame()
		end
	end

	--check for a position button being held down
	timeLeft = timeLeft - elapsed
	if timeLeft > 0 then return end-- cut out early
	timeLeft = TIME_DELAY -- reset to TIME_DELAY seconds
	-- this will run roughly every TIME_DELAY seconds/milliseconds/whatever
	xHUD.PosButtonPressed(direction)
end
