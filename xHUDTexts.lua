LibGUI = LibStub("LibGUI")
local F = {}

local FLAG_PLAYER_CUR_HP = 1
local FLAG_PLAYER_MAX_HP = 2
local FLAG_PLAYER_PERCENT_HP = 3
local FLAG_PLAYER_DIFF_HP = 4
local FLAG_PLAYER_CUR_AP = 5
local FLAG_PLAYER_MAX_AP = 6
local FLAG_PLAYER_PERCENT_AP = 7
local FLAG_PLAYER_CUR_CAREER = 8
local FLAG_PLAYER_MAX_CAREER = 9
local FLAG_PLAYER_PERCENT_CAREER = 10
local FLAG_FRIENDLY_PERCENT_HP = 11
local FLAG_ENEMY_PERCENT_HP = 12
local FLAG_PLAYER_NAME = 13
local FLAG_FRIENDLY_NAME = 14
local FLAG_ENEMY_NAME = 15
local FLAG_PLAYER_LEVEL = 16
local FLAG_FRIENDLY_LEVEL = 17
local FLAG_ENEMY_LEVEL = 18
local FLAG_FRIENDLY_TIER = 19
local FLAG_ENEMY_TIER = 20
local FLAG_PLAYER_CUR_MORALE = 21
local FLAG_PLAYER_MAX_MORALE = 22
local FLAG_PLAYER_MORALE_LEVEL = 23
local FLAG_PLAYER_PERCENT_MORALE = 24
local FLAG_PLAYER_CUR_XP = 25
local FLAG_PLAYER_NEEDED_XP = 26
local FLAG_PLAYER_RESTED_XP = 27
local FLAG_PLAYER_RENOWN_RANK = 28
local FLAG_PLAYER_CUR_RP = 29
local FLAG_PLAYER_NEEDED_RP = 30
local FLAG_PLAYER_CUR_INF = 31
local FLAG_PLAYER_NEEDED_INF = 32
local FLAG_PLAYER_STEP_INF = 33
local FLAG_CUR_CASTTIME = 34
local FLAG_MAX_CASTTIME = 35
local FLAG_PUSHBACK_CASTTIME = 36
local FLAG_SPELL = 37

local tags = {
	[FLAG_PLAYER_CUR_HP] = "cur_hp",
	[FLAG_PLAYER_MAX_HP] = "max_hp",
	[FLAG_PLAYER_PERCENT_HP] = "percent_hp",
	[FLAG_PLAYER_DIFF_HP] = "diff_hp",
	[FLAG_PLAYER_CUR_AP] = "cur_ap",
	[FLAG_PLAYER_MAX_AP] = "max_ap",
	[FLAG_PLAYER_PERCENT_AP] = "percent_ap",
	[FLAG_PLAYER_CUR_CAREER] = "cur_career",
	[FLAG_PLAYER_MAX_CAREER] = "max_career",
	[FLAG_PLAYER_PERCENT_CAREER] = "percent_career",
	[FLAG_FRIENDLY_PERCENT_HP] = "friend_hp",
	[FLAG_ENEMY_PERCENT_HP] = "enemy_hp",
	[FLAG_PLAYER_NAME] = "player_name",
	[FLAG_FRIENDLY_NAME] = "friend_name",
	[FLAG_ENEMY_NAME] = "enemy_name",
	[FLAG_PLAYER_LEVEL] = "player_level",
	[FLAG_FRIENDLY_LEVEL] = "friend_level",
	[FLAG_ENEMY_LEVEL] = "enemy_level",
	[FLAG_FRIENDLY_TIER] = "friend_tier",
	[FLAG_ENEMY_TIER] = "enemy_tier",
	[FLAG_PLAYER_CUR_MORALE] = "cur_morale",
	[FLAG_PLAYER_MAX_MORALE] = "max_morale",
	[FLAG_PLAYER_MORALE_LEVEL] = "morale_level",
	[FLAG_PLAYER_PERCENT_MORALE] = "percent_morale",
	[FLAG_PLAYER_CUR_XP] = "cur_xp",
	[FLAG_PLAYER_NEEDED_XP] = "needed_xp",
	[FLAG_PLAYER_RESTED_XP] = "rested_xp",
	[FLAG_PLAYER_RENOWN_RANK] = "renown_rank",
	[FLAG_PLAYER_CUR_RP] = "cur_rp",
	[FLAG_PLAYER_NEEDED_RP] = "needed_rp",
	[FLAG_PLAYER_CUR_INF] = "cur_inf",
	[FLAG_PLAYER_NEEDED_INF] = "needed_inf",
	[FLAG_PLAYER_STEP_INF] = "step_inf",
	[FLAG_CUR_CASTTIME] = "cur_casttime",
	[FLAG_MAX_CASTTIME] = "max_casttime",
	[FLAG_PUSHBACK_CASTTIME] = "pushback_casttime",
	[FLAG_SPELL] = "spell",
	}

local funs = {
	[FLAG_PLAYER_CUR_HP] = function() return GameData.Player.hitPoints.current end,
	[FLAG_PLAYER_MAX_HP] = function() return GameData.Player.hitPoints.maximum end,
	[FLAG_PLAYER_PERCENT_HP] = function() return math.floor(GameData.Player.hitPoints.current/GameData.Player.hitPoints.maximum*100) end,
	[FLAG_PLAYER_DIFF_HP] = function() return GameData.Player.hitPoints.maximum-GameData.Player.hitPoints.current end,
	[FLAG_PLAYER_CUR_AP] = function() return GameData.Player.actionPoints.current end,
	[FLAG_PLAYER_MAX_AP] = function() return GameData.Player.actionPoints.maximum end,
	[FLAG_PLAYER_PERCENT_AP] = function() return math.floor(GameData.Player.actionPoints.current/GameData.Player.actionPoints.maximum*100) end,
	[FLAG_PLAYER_CUR_CAREER] = function() return xHUD.cur_career end,
	[FLAG_PLAYER_MAX_CAREER] = function() return xHUD.max_career end,
	[FLAG_PLAYER_PERCENT_CAREER] = function() return math.floor(xHUD.cur_career/xHUD.max_career*100) end,
	[FLAG_FRIENDLY_PERCENT_HP] = function() return TargetInfo:UnitHealth("selffriendlytarget") end,
	[FLAG_ENEMY_PERCENT_HP] = function() return TargetInfo:UnitHealth("selfhostiletarget") end,
	[FLAG_PLAYER_NAME] = function() return tostring(GameData.Player.name) end,
	[FLAG_FRIENDLY_NAME] = function() return tostring(TargetInfo:UnitName("selffriendlytarget")) end,
	[FLAG_ENEMY_NAME] = function() return tostring(TargetInfo:UnitName("selfhostiletarget")) end,
	[FLAG_PLAYER_LEVEL] = function() return GameData.Player.level end,
	[FLAG_FRIENDLY_LEVEL] = function() return TargetInfo:UnitLevel("selffriendlytarget") end,
	[FLAG_ENEMY_LEVEL] = function() return TargetInfo:UnitLevel("selfhostiletarget") end,
	[FLAG_FRIENDLY_TIER] =
		 function()
			 local t = TargetInfo:UnitTier("selffriendlytarget")
			 if t == 0 then
					return ""
			 elseif t == 1 then
				 return "+"
			 elseif t == 2 then
				 return "++"
			 elseif t == 3 then
				 return "+++"
			 else
				 return "++++"
			 end
		 end,
	[FLAG_ENEMY_TIER] =
		 function()
			 local t = TargetInfo:UnitTier("selfhostiletarget")
			 if t == 0 then
					return ""
			 elseif t == 1 then
				 return "+"
			 elseif t == 2 then
				 return "++"
			 elseif t == 3 then
				 return "+++"
			 else
				 return "++++"
			 end
		 end,
	[FLAG_PLAYER_CUR_MORALE] = function() return xHUD.cur_morale end,
	[FLAG_PLAYER_MAX_MORALE] = function() return xHUD.max_morale end,
	[FLAG_PLAYER_MORALE_LEVEL] = function() return xHUD.morale_level end,
	[FLAG_PLAYER_PERCENT_MORALE] = function() return math.floor(xHUD.cur_morale/xHUD.max_morale*100) end,
	[FLAG_PLAYER_CUR_XP] = function() return GameData.Player.Experience.curXpEarned end,
	[FLAG_PLAYER_NEEDED_XP] = function() return GameData.Player.Experience.curXpNeeded end,
	[FLAG_PLAYER_RESTED_XP] = function() return GameData.Player.Experience.restXp end,
	[FLAG_PLAYER_RENOWN_RANK] = function() return GameData.Player.Renown.curRank end,
	[FLAG_PLAYER_CUR_RP] = function() return GameData.Player.Renown.curRenownEarned end,
	[FLAG_PLAYER_NEEDED_RP] = function() return GameData.Player.Renown.curRenownNeeded end,
	[FLAG_PLAYER_CUR_INF] =
		function()
			if xHUD.InfData then
				return xHUD.InfData.curValue
			else
				return ""
			end
		end,
	[FLAG_PLAYER_NEEDED_INF] =
		function()
			if xHUD.InfData then
				for i=1,3 do
					if xHUD.InfData.curValue <= xHUD.InfData.rewardLevel[i].amountNeeded then
						return xHUD.InfData.rewardLevel[i].amountNeeded - xHUD.InfData.curValue
					end
				end
			else
				return ""
			end
		end,
	[FLAG_PLAYER_STEP_INF] =
		function()
			if xHUD.InfData then
				for i=1,3 do
					if xHUD.InfData.curValue <= xHUD.InfData.rewardLevel[i].amountNeeded then
						return i
					end
				end
			else
				return ""
			end
		end,
	[FLAG_CUR_CASTTIME] = function() return xHUD.curCastTime end,
	[FLAG_MAX_CASTTIME] = function() return xHUD.maxCastTime end,
	[FLAG_PUSHBACK_CASTTIME] = function() return xHUD.curPushBack end,
	[FLAG_SPELL] = function() return xHUD.curSpell end,
				}

function xHUD.CreateNewTextPanel(name)
	if xHUD.Settings.Texts[name] then return end

	local settings = {}
	xHUD.Settings.Texts[name] = settings

	settings.width = 200
	settings.height = 25
	settings.font = "font_clear_medium_bold"
	settings.align = "center"
	settings.color = {255,255,255}
	settings.text = ""
	settings.x = 500
	settings.y = 300
	settings.scale = 0.72
	settings.parent = "Root"

	xHUD.RecreateTexts()
end

function xHUD.SaveTextPositions()
	for text,val in pairs(xHUD.Settings.Texts) do
		local x,y = WindowGetScreenPosition(F[text].name)
		local w,h = WindowGetDimensions(F[text].name)
		val.scale = WindowGetScale(F[text].name)
		val.x = x
		val.y = y
		val.width = w
		val.height = h
	end
end

function xHUD.RecreateTexts()
	local scale = InterfaceCore.GetScale()

	for key,val in pairs(xHUD.Settings.Texts) do
		val.line, val.flags = xHUD.ConvertTextString(val.text)

		if not F[key] then
			F[key] = LibGUI("Label")
		end
		local B = F[key]
		B:Parent(xHUD.FrameHandle()[val.parent])
		B:Resize(val.width, val.height)
		B:Scale(val.scale)
		B:AnchorTo("Root","topleft","topleft",val.x/scale, val.y/scale)
		B:Color(unpack(val.color))
		B:Font(val.font)
		B:Align(val.align)

		LayoutEditor.UnregisterWindow(B.name)
		if val.layouteditor then
			LayoutEditor.RegisterWindow(B.name,towstring(key),"",true,true,true)
		end

		xHUD.UpdateText(key)
	end
end

function xHUD.ConvertTextString(str)
	local result = {}
	local flags = {}
	local init = 1

	while(1) do
		local i,j = string.find(str,"%$",init)
		local t = string.sub(str,init,(i and i-1) or nil)
		if t ~= "" then
			table.insert(result,t)
		end
		if not i then
			return result, flags
		else
			init = i+1
			for k,tag in pairs(tags) do
				local len = string.len(tag)
				if string.sub(str,init,init+len-1) == tag then
					table.insert(result,k)
					flags[k] = true
					init = init+len
					break
				end
			end
		end
	end
end

function xHUD.UpdateText(label)
	local str = ""
	for i,val in ipairs(xHUD.Settings.Texts[label].line) do
		if type(val) == "string" then
			str = str..val
		else
			str = str..funs[val]()
		end
	end
	F[label]:SetText(str)
end

function xHUD.UpdateFlags(flags)
	local update = {}

	if type(flags) == "number" then
		flags = {flags}
	end

	for key,val in pairs(xHUD.Settings.Texts) do
		for i,flag in ipairs(flags) do
			if val.flags[flag] == true then
				table.insert(update,key)
				break
			end
		end
	end

	for i,label in ipairs(update) do
		xHUD.UpdateText(label)
	end
end

function xHUD.UpdateTargetText(unit)
	if unit == "selffriendlytarget" then
		xHUD.UpdateFlags({FLAG_FRIENDLY_PERCENT_HP, FLAG_FRIENDLY_NAME, FLAG_FRIENDLY_LEVEL, FLAG_FRIENDLY_TIER})
	else
		xHUD.UpdateFlags({FLAG_ENEMY_PERCENT_HP, FLAG_ENEMY_NAME, FLAG_ENEMY_LEVEL, FLAG_ENEMY_TIER})
	end
end

function xHUD.UpdatePlayerHPText()
	xHUD.UpdateFlags({FLAG_PLAYER_CUR_HP, FLAG_PLAYER_MAX_HP, FLAG_PLAYER_PERCENT_HP, FLAG_PLAYER_DIFF_HP})
end

function xHUD.UpdatePlayerAPText()
	xHUD.UpdateFlags({FLAG_PLAYER_CUR_AP, FLAG_PLAYER_MAX_AP, FLAG_PLAYER_PERCENT_AP})
end

function xHUD.UpdatePlayerCareerText()
	xHUD.UpdateFlags({FLAG_PLAYER_CUR_CAREER, FLAG_PLAYER_MAX_CAREER, FLAG_PLAYER_PERCENT_CAREER})
end

function xHUD.UpdatePlayerMoraleText()
	xHUD.UpdateFlags({FLAG_PLAYER_CUR_MORALE, FLAG_PLAYER_MAX_MORALE, FLAG_PLAYER_MORALE_LEVEL, FLAG_PLAYER_PERCENT_MORALE})
end

function xHUD.UpdatePlayerExperienceText()
	xHUD.UpdateFlags({FLAG_PLAYER_CUR_XP, FLAG_PLAYER_NEEDED_XP, FLAG_PLAYER_RENOWN_RANK, FLAG_PLAYER_LEVEL})
end

function xHUD.UpdatePlayerRenownText()
	xHUD.UpdateFlags({FLAG_PLAYER_RENOWN_RANK, FLAG_PLAYER_CUR_RP, FLAG_PLAYER_NEEDED_RP})
end

function xHUD.UpdatePlayerInfluenceText()
	xHUD.UpdateFlags({FLAG_PLAYER_CUR_INF, FLAG_PLAYER_NEEDED_INF, FLAG_PLAYER_STEP_INF})
end
