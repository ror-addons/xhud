LibGUI = LibStub("LibGUI")

local F = {}
F.Anchors = {}

function xHUD.SaveBuffPositions()
	for i,key in ipairs(xHUD.BuffList) do
		local x,y = WindowGetScreenPosition(F.Anchors[key].name)
		xHUD.Settings[key].x = x
		xHUD.Settings[key].y = y
	end
end

function xHUD.ClearBuffs(targetClassification)
	local key

	if targetClassification == "selffriendlytarget" then
		key = "FriendlyTargetBuffs"
	else
		key = "HostileTargetBuffs"
	end

	xHUD.Settings[key].counter = 0
	if F[key] then
		for k,_ in pairs(F[key]) do
			F[key][k]:Hide()
		end
	end
	xHUD.Buffdata[key] = {}
end

function xHUD.RecreateBuffs()
	local scale = InterfaceCore.GetScale()

	for _,key in ipairs(xHUD.BuffList) do
		val = xHUD.Settings[key]
		
		xHUD.Buffdata[key] = {}

		if not F.Anchors[key] then
			F.Anchors[key] = LibGUI("Window")
		end

		B = F.Anchors[key]
		B:Resize(val.size, val.size)
		B:IgnoreInput()
		B:Hide()
		B:Position(val.x/scale, val.y/scale)

		LayoutEditor.UnregisterWindow(B.name)
		if val.layouteditor then
			LayoutEditor.RegisterWindow(B.name,towstring(key),"",false,false,true)
		end

		if F[key] then
			for k,_ in pairs(F[key]) do
				F[key][k]:Destroy()
			end
		end
		F[key] = {}
	end
end

function xHUD.UpdateBuffTimers(elapsed)
	for _,key in ipairs(xHUD.BuffList) do
		for k,v in pairs(xHUD.Buffdata[key]) do
			if xHUD.Buffdata[key][k].duration ~= 0 then
				xHUD.Buffdata[key][k].duration = xHUD.Buffdata[key][k].duration - elapsed
				if xHUD.Buffdata[key][k].duration < -5 then
					xHUD.Buffdata[key][k] = nil
					xHUD.RenderBuffs(key)
				else
					xHUD.Buffdata[key][k].frame.Timer:SetText(TimeUtils.FormatTimeCondensed(xHUD.Buffdata[key][k].duration))
				end
			end
		end
	end
end

function xHUD.UpdateBuffs(updateType, effects)
	local data
	local settings
	local typ

	if type(effects) == "boolean" then
		effects = updateType
		typ = "PlayerBuffs"
	elseif updateType == GameData.BuffTargetType.TARGET_FRIENDLY then
		typ = "FriendlyTargetBuffs"
	else
		typ = "HostileTargetBuffs"
	end

	data = xHUD.Buffdata[typ]
	settings = xHUD.Settings[typ]

	if not settings.enabled then return end

	for k,v in pairs(effects) do
		if v.name and (not settings.selfbuff or v.castByPlayer) then
			data[k] = {}
			data[k].data = v
			data[k].duration = v.duration
		else
			data[k] = nil
		end
	end

	xHUD.RenderBuffs(typ)
end

function xHUD.RenderBuffs(typ)
	local i = 0
	local B

	data = xHUD.Buffdata[typ]
	settings = xHUD.Settings[typ]

	for k,v in pairs(data) do
		i = i + 1
		if not F[typ][i] then
			F[typ][i] = LibGUI("Window")
			B = F[typ][i]
			B:Resize(settings.size, settings.size)
			if not settings.clickthrough then
				B:RegisterDefaultEvents()
			end
			B.OnMouseOver = function() xHUD.BuffsOnMouseOver(B) end
			B.OnRButtonUp = function() xHUD.BuffsOnRButtonUp(B) end

			B.Background = B("Image")
			B.Background:Resize(settings.size, settings.size)
			B.Background:Layer("background")
			B.Background:IgnoreInput()

			B.Icon = B("Image")
			B.Icon:Resize(settings.size * 0.9, settings.size * 0.9)
			B.Icon:AnchorTo(B,"center","center")
			B.Icon:Layer("secondary")
			B.Icon:TexDims(64,64)
			B.Icon:IgnoreInput()

			B.Timer = B("Label")
			B.Timer:Resize(40,20)
			B.Timer:Scale(0.8*settings.size/40)
			B.Timer:AnchorTo(B,"bottom","bottom")
			B.Timer:Align("center")
			B.Timer:Font("font_clear_medium")
			B.Timer:Color(255,255,255)
			B.Timer:Layer("overlay")
			B.Timer:IgnoreInput()

			B.Count = B("Label")
			B.Count:Resize(40,20)
			B.Count:Scale(0.8*settings.size/40)
			B.Count:AnchorTo(B,"topright","topright")
			B.Count:Align("right")
			B.Count:Font("font_clear_medium")
			B.Count:Color(255,255,255)
			B.Count:Layer("overlay")
			B.Count:IgnoreInput()

			local dx, dy = 0,0
			local x = i%settings.max - 1
			local y = math.floor((i-1)/settings.max)
			local row,col
			if x < 0 then
				x = settings.max - 1
			end

			if settings.direction1 == "up" then
				row = -x
				dx = settings.arc * math.sin(math.pi*x/(settings.max-1))
			elseif settings.direction1 == "down" then
				row = x
				dx = settings.arc * math.sin(math.pi*x/(settings.max-1))
			elseif settings.direction1 == "right" then
				col = x
				dy = settings.arc * math.sin(math.pi*y/(settings.max-1))
			else
				col = -x
				dy = settings.arc * math.sin(math.pi*y/(settings.max-1))
			end

			if settings.direction2 == "up" then
				row = -y
			elseif settings.direction2 == "down" then
				row = y
			elseif settings.direction2 == "right" then
				col = y
			else
				col = -y
			end

			B:AnchorTo(F.Anchors[typ],"topleft","topleft",col*settings.size+dx, row*settings.size+dy)

			if settings.clickthrough then
				B:IgnoreInput()
			else
				B:CaptureInput()
			end
		else
			B = F[typ][i]
		end

		if B.data ~= v.data then
			data[k].frame = B
			B.data = v.data
			B.own = (typ == "PlayerBuffs")

			local texture = GetIconData(v.data.iconNum)
			B.Icon:Texture(texture)
			B.Background:Tint(v.data.typeColorRed or 0, v.data.typeColorGreen or 0, v.data.typeColorBlue or 0)
			if v.data.duration > 0 then
				B.Timer:SetText(TimeUtils.FormatTimeCondensed(v.duration))
			else
				B.Timer:SetText("")
			end
			if v.data.stackCount > 1 then
				B.Count:SetText("x"..v.data.stackCount)
			else
				B.Count:SetText("")
			end

			B:Show()
		end
	end

	for j=i+1, settings.counter do
		if F[typ][j] then
			F[typ][j]:Hide()
		end
	end
	settings.counter = i
end

-- mirror the functions of BuffFrame (or do some rewriting stuff)
function xHUD.BuffsOnRButtonUp(frame)
	if frame.own then
		RemoveEffect(frame.data.effectIndex)
	end
end

function xHUD.BuffsOnMouseOver(frame)
	local buffData = frame.data

	Tooltips.CreateTextOnlyTooltip (SystemData.ActiveWindow.name, nil) 
	Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_HEADING)
	Tooltips.SetTooltipColorDef (1, 2, Tooltips.COLOR_HEADING)
	Tooltips.SetTooltipActionText (GetString (StringTables.Default.TEXT_R_CLICK_TO_REMOVE_EFFECT))

	local durationText = TimeUtils.FormatTimeCondensed (buffData.duration)
	Tooltips.SetTooltipText (1, 1, buffData.name)
	Tooltips.SetTooltipText (3, 1, buffData.effectText)

	-- buffData is not an abilityData, but the fields required by this function should be present...
	Tooltips.SetTooltipText (1, 2, DataUtils.GetAbilityTypeText (buffData))

	if buffData.duration > 1 then
		Tooltips.SetTooltipText (2, 1, durationText)
	else
		Tooltips.SetTooltipText (2, 1, L"")
	end

	Tooltips.Finalize()

	local tooltip_anchor = { Point = "bottom",	RelativeTo = SystemData.ActiveWindow.name, RelativePoint = "top",	 XOffset = 0, YOffset = 20 }
	Tooltips.AnchorTooltip (tooltip_anchor)
end
