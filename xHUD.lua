LibGUI = LibStub("LibGUI")
if not xHUD then xHUD = {} end -- modif

xHUD.Textures = {
	xHUD_LeftRoundBar = {128,512}, xHUD_RightRoundBar = {128,512},
	xHUD_LeftGlowArc = {128,512}, xHUD_RightGlowArc = {128,512},
	xHUD_SancOrb = {256,256}, xHUD_SancCenter = {256,256},
	xHUD_LeftMetal = {128,512}, xHUD_RightMetal = {128,512},
	xHUD_LeftPlain = {128,512}, xHUD_RightPlain = {128,512},
	xHUD_HorizontalFuzzyBar = {256,32}, xHUD_VerticalFuzzyBar = {32,256},
	}

local DefaultSettings = {
	version = 2.0, smoothDelay = 0.5, updateDelay = 0.5,
	["xHUDPlayerHP"] = {
			enabled = true,
			x = 350,
			y = 315,
			width = 200,
			height = 400,
			direction = "up",
			smooth = false,
			inverse = false,
			color = {0,255,0},
			bgcolor = {0,0,0},
			alpha = 1,
			bgalpha = 0.2,
			ooc_alpha = 0,
			no_target_fade = true,
			clickthrough = false,
			layouteditor = false,
			texture = "xHUD_LeftGlowArc",
			},
	["xHUDPlayerAP"] = {
			enabled = true,
			x = 365,
			y = 315,
			width = 200,
			height = 400,
			direction = "up",
			smooth = true,
			inverse = false,
			color = {255,255,0},
			bgcolor = {0,0,0},
			alpha = 1,
			bgalpha = 0.2,
			ooc_alpha = 0,
			no_target_fade = true,
			clickthrough = false,
			layouteditor = false,
			texture = "xHUD_LeftGlowArc",
			},
	["xHUDPlayerCareer"] = {
			enabled = true,
			x = 380,
			y = 315,
			width = 200,
			height = 400,
			direction = "up",
			smooth = false,
			inverse = false,
			color = {200,100,255},
			color2 = {255,100,50},
			color3 = {185,1,1},
			bgcolor = {0,0,0},
			alpha = 1,
			bgalpha = 0.2,
			ooc_alpha = 0,
			no_target_fade = true,
			clickthrough = true,
			layouteditor = false,
			texture = "xHUD_LeftGlowArc",
			},
	["xHUDPlayerMorale"] = {
			enabled = false,
			x = 395,
			y = 315,
			width = 200,
			height = 400,
			direction = "up",
			smooth = false,
			inverse = false,
			color = {0,0,255},
			color2 = {150,150,255},
			bgcolor = {0,0,0},
			alpha = 1,
			bgalpha = 0.2,
			ooc_alpha = 0,
			no_target_fade = true,
			clickthrough = true,
			layouteditor = false,
			texture = "xHUD_LeftGlowArc",
			},
	["xHUDPlayerXP"] = {
			enabled = false,
			x = 410,
			y = 315,
			width = 200,
			height = 400,
			direction = "up",
			smooth = false,
			inverse = false,
			color = {0,255,150},
			color2 = {0,0,0},
			bgcolor = {0,0,0},
			alpha = 1,
			bgalpha = 0.2,
			ooc_alpha = 0,
			no_target_fade = true,
			clickthrough = true,
			layouteditor = false,
			texture = "xHUD_LeftGlowArc",
			 },
	["xHUDPlayerRP"] = {
			enabled = false,
			x = 425,
			y = 315,
			width = 200,
			height = 400,
			direction = "up",
			smooth = false,
			inverse = false,
			color = {180,0,230},
			bgcolor = {0,0,0},
			alpha = 1,
			bgalpha = 0.2,
			ooc_alpha = 0,
			no_target_fade = true,
			clickthrough = true,
			layouteditor = false,
			texture = "xHUD_LeftGlowArc",
			},
	["xHUDPlayerInf"] = {
			enabled = false,
			x = 440,
			y = 315,
			width = 200,
			height = 400,
			direction = "up",
			smooth = false,
			inverse = false,
			color = {0,0,255},
			bgcolor = {0,0,0},
			alpha = 1,
			bgalpha = 0.2,
			ooc_alpha = 0,
			no_target_fade = true,
			clickthrough = true,
			layouteditor = false,
			texture = "xHUD_LeftGlowArc",
			},
	["xHUDCastBar"] = {
			enabled = false,
			x = 470,
			y = 590,
			width = 300,
			height = 20,
			direction = "right",
			smooth = false,
			inverse = false,
			color = {255,255,255},
			bgcolor = {0,0,0},
			alpha = 1,
			bgalpha = 0.2,
			ooc_alpha = 1,
			no_target_fade = true,
			clickthrough = true,
			layouteditor = false,
			texture = "xHUD_HorizontalFuzzyBar",
			}, 
	["xHUDFriendlyTargetHP"] = {
			enabled = true,
			x = 655,
			y = 315,
			width = 200,
			height = 400,
			direction = "up",
			smooth = false,
			inverse = false,
			color = {100,100,255},
			bgcolor = {0,0,0},
			alpha = 1,
			bgalpha = 0.2,
			ooc_alpha = 0,
			no_target_fade = true,
			clickthrough = false,
			layouteditor = false,
			uselti = false,
			texture = "xHUD_RightGlowArc",
			},
	["xHUDHostileTargetHP"] = {
			enabled = true,
			x = 670,
			y = 315,
			width = 200,
			height = 400,
			direction = "up",
			smooth = false,
			inverse = false,
			color = {255,0,0},
			bgcolor = {0,0,0},
			alpha = 1,
			bgalpha = 0.2,
			ooc_alpha = 0,
			no_target_fade = true,
			clickthrough = true,
			layouteditor = false,
			uselti = false,
			texture = "xHUD_RightGlowArc",
			},
	["xHUDFriendlyTargetIcon"] = {
			enabled = true,
			x = 695,
			y = 350,
			scale = 0.34,
			layouteditor = false,
			parent = "xHUDFriendlyTargetHP",
			},
	["xHUDHostileTargetIcon"] = {
			enabled = true,
			x = 710,
			y = 350,
			scale = 0.34,
			layouteditor = false,
			parent = "xHUDHostileTargetHP",
			},		
	["PlayerBuffs"] = {
			enabled = true,
			size = 40,
			unit = GameData.BuffTargetType.SELF,
			frame = "xHUDPlayerBuff",
			x = 420,
			y = 575,
			direction1 = "up",
			direction2 = "left",
			max = 10,
			arc = -75,
			counter = 0,
			alpha = 1,
			ooc_alpha = 0,
			no_target_fade = true,
			clickthrough = false,
			selfbuff = false,
			layouteditor = false,
			},
	["FriendlyTargetBuffs"] = {
			enabled = true,
			size = 40,
			unit = GameData.BuffTargetType.TARGET_FRIENDLY,
			frame = "xHUDFriendlyTargetBuff",
			x = 640,
			y = 575,
			direction1 = "up",
			direction2 = "left",
			max = 10,
			arc = 75,
			counter = 0,
			alpha = 1,
			ooc_alpha = 0,
			no_target_fade = true,
			clickthrough = false,
			selfbuff = false,
			layouteditor = false,
			},
	["HostileTargetBuffs"] = {
			enabled = true,
			size = 40,
			unit = GameData.BuffTargetType.TARGET_HOSTILE,
			frame = "xHUDHostileTargetBuff",
			x = 715,
			y = 575,
			direction1 = "up",
			direction2 = "right",
			max = 10,
			arc = 75,
			counter = 0,
			alpha = 1,
			ooc_alpha = 0,
			no_target_fade = true,
			clickthrough = false,
			selfbuff = false,
			layouteditor = false,
			},
	["Texts"] = {
		["PlayerHP"] = {
			text = "$cur_hp/$max_hp",
			width = 200,
			height = 25,
			x = 420,
			y = 604,
			scale = 0.72,
			color = {0,255,0},
			align = "center",
			font = "font_clear_medium_bold",
			parent = "xHUDPlayerHP",
			layouteditor = false,
			},
		["PlayerAP"] = {
			text = "$cur_ap/$max_ap",
			width = 200,
			height = 25,
			x = 420,
			y = 622,
			scale = 0.72,
			color = {255,255,0},
			align = "center",
			font = "font_clear_medium_bold",
			parent = "xHUDPlayerAP",
			layouteditor = false,
			},
		["PlayerCareer"] = {
			text = "$cur_career",
			width = 200,
			height = 25,
			x = 420,
			y = 636,
			scale = 0.72,
			color = {200,100,255},
			align = "center",
			font = "font_clear_medium_bold",
			parent = "xHUDPlayerCareer",
			layouteditor = false,
			},
		["FriendName"] = {
			text = "[$friend_level$friend_tier] $friend_name ($friend_hp%)",
			width = 350,
			height = 30,
			x = 564,
			y = 604,
			scale = 0.72,
			color = {100,100,255},
			align = "center",
			font = "font_clear_medium_bold",
			parent = "xHUDFriendlyTargetHP",
			layouteditor = false,
			},
		["EnemyName"] = {
			text = "[$enemy_level$enemy_tier] $enemy_name ($enemy_hp%)",
			width = 350,
			height = 30,
			x = 564,
			y = 622,
			scale = 0.72,
			color = {255,0,0},
			align = "center",
			font = "font_clear_medium_bold",
			parent = "xHUDHostileTargetHP",
			layouteditor = false,
			},
		},
	}

xHUD.Profiles = {}

xHUD.BarList = {
	"xHUDPlayerHP",
	"xHUDPlayerAP",
	"xHUDPlayerMorale",
	"xHUDPlayerCareer",
	"xHUDFriendlyTargetHP",
	"xHUDHostileTargetHP",
	"xHUDPlayerXP",
	"xHUDPlayerRP",
	"xHUDPlayerInf",
	"xHUDCastBar",
	}

xHUD.IconList = {
	"xHUDFriendlyTargetIcon",
	"xHUDHostileTargetIcon",
	}

xHUD.BuffList = {
	"PlayerBuffs",
	"FriendlyTargetBuffs",
	"HostileTargetBuffs",
	}


xHUD.Buffdata = {}

local updateTime = 0
local updateSettings = 1
local smoothTable = {}
local initialized = false
local F = {}

local function print(msg)
	EA_ChatWindow.Print(towstring(msg))
end

-- from http://lua-users.org/wiki/CopyTable
function xHUD.deepcopy(object)
	local lookup_table = {}
	local function _copy(object)
		if type(object) ~= "table" then
			return object
		elseif lookup_table[object] then
			return lookup_table[object]
		end
		local new_table = {}
		lookup_table[object] = new_table
		for index, value in pairs(object) do
			new_table[_copy(index)] = _copy(value)
		end
		return setmetatable(new_table, getmetatable(object))
	end
	return _copy(object)
end

-- adds the values of tab2 to tab1, if not present
local function addtable(tab1, tab2)
	for key,val in pairs(tab2) do
		if type(val) == "table" then
			if not tab1[key] or type(tab1[key]) ~= "table" then
				tab1[key] = {}
			end
			addtable(tab1[key],val)
		end
		if not tab1[key] then
			tab1[key] = val
		end
	end
end

function xHUD.Initialize()
	d("xHUD initialized.")

	RegisterEventHandler(SystemData.Events.PLAYER_CUR_HIT_POINTS_UPDATED, "xHUD.UpdatePlayerHP")
	RegisterEventHandler(SystemData.Events.PLAYER_MAX_HIT_POINTS_UPDATED, "xHUD.UpdatePlayerHP")
	RegisterEventHandler(SystemData.Events.PLAYER_CUR_ACTION_POINTS_UPDATED, "xHUD.UpdatePlayerAP")
	RegisterEventHandler(SystemData.Events.PLAYER_MAX_ACTION_POINTS_UPDATED, "xHUD.UpdatePlayerAP")
	RegisterEventHandler(SystemData.Events.PLAYER_CAREER_RESOURCE_UPDATED, "xHUD.UpdatePlayerCareer")
	RegisterEventHandler(SystemData.Events.PLAYER_MORALE_UPDATED, "xHUD.UpdatePlayerMorale")
	RegisterEventHandler(SystemData.Events.PLAYER_TARGET_UPDATED, "xHUD.UpdateTargets")
	RegisterEventHandler(SystemData.Events.PLAYER_EFFECTS_UPDATED, "xHUD.UpdateBuffs")
	RegisterEventHandler(SystemData.Events.PLAYER_TARGET_EFFECTS_UPDATED, "xHUD.UpdateBuffs")
	RegisterEventHandler(SystemData.Events.PLAYER_PET_HEALTH_UPDATED, "xHUD.UpdatePetHealth")
	RegisterEventHandler(SystemData.Events.PLAYER_PET_STATE_UPDATED, "xHUD.UpdatePetHealth")
	RegisterEventHandler(SystemData.Events.PLAYER_EXP_UPDATED, "xHUD.UpdatePlayerXP")
	RegisterEventHandler(SystemData.Events.PLAYER_RENOWN_UPDATED, "xHUD.UpdatePlayerRP")
	RegisterEventHandler(SystemData.Events.PLAYER_INFLUENCE_UPDATED, "xHUD.UpdatePlayerInf")
	RegisterEventHandler(SystemData.Events.PUBLIC_QUEST_ADDED, "xHUD.UpdatePlayerInf")
	RegisterEventHandler(SystemData.Events.PUBLIC_QUEST_UPDATED, "xHUD.UpdatePlayerInf")
	RegisterEventHandler(SystemData.Events.PUBLIC_QUEST_REMOVED, "xHUD.UpdatePlayerInf")
	RegisterEventHandler(SystemData.Events.PLAYER_AREA_CHANGED, "xHUD.UpdatePlayerInf")
	RegisterEventHandler(SystemData.Events.PLAYER_START_INTERACT_TIMER, "xHUD.UpdateInteractTimer")
	RegisterEventHandler(SystemData.Events.INTERACT_DONE, "xHUD.EndCastTime")
	RegisterEventHandler(SystemData.Events.PLAYER_BEGIN_CAST, "xHUD.UpdateCastTime")
	RegisterEventHandler(SystemData.Events.PLAYER_END_CAST, "xHUD.EndCastTime")
	RegisterEventHandler(SystemData.Events.PLAYER_CAST_TIMER_SETBACK, "xHUD.SetBackCastTime")
	RegisterEventHandler(SystemData.Events.LOADING_END, "xHUD.LoadingEnd")
	RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "xHUD.LoadingEnd")

	LibSlash.RegisterSlashCmd("xhud", xHUD.Slash)
	table.insert(LayoutEditor.EventHandlers, xHUD.SavePositions)

	xHUD.UpdateSettings()
	xHUD.Recreate()
end

function xHUD.UpdateSettings()
	if not xHUD.Settings then xHUD.Settings = xHUD.deepcopy(DefaultSettings) return end

	if not xHUD.Settings.version or xHUD.Settings.version < DefaultSettings.version then
		addtable(xHUD.Settings, DefaultSettings)
		-- convert some settings to boolean after 1.7 and remove Buffdata
		if xHUD.Settings.version <= 1.7 then
			xHUD.Settings.Buffdata = nil

			for i,val in ipairs(xHUD.BarList) do
				xHUD.Settings[val].enabled = (xHUD.Settings[val].enabled == 1)
				xHUD.Settings[val].no_target_fade = (xHUD.Settings[val].no_target_fade == 1)
				xHUD.Settings[val].clickthrough = (xHUD.Settings[val].clickthrough == 1)
			end
			for i,val in ipairs(xHUD.BuffList) do
				xHUD.Settings[val].enabled = (xHUD.Settings[val].enabled == 1)
				xHUD.Settings[val].update = (xHUD.Settings[val].update == 1)
				xHUD.Settings[val].clickthrough = (xHUD.Settings[val].clickthrough == 1)
				xHUD.Settings[val].no_target_fade = (xHUD.Settings[val].no_target_fade == 1)
				xHUD.Settings[val].selfbuff = (xHUD.Settings[val].selfbuff == 1)
			end
		end
		xHUD.Settings.version = DefaultSettings.version
	end
end

function xHUD.RevertToDefault()
	xHUD.Settings = xHUD.deepcopy(DefaultSettings)
	print("xHUD: all settings have been reverted to Default")
	xHUD.Recreate()
end

function xHUD.Slash()
	print("Opening xHUD configuration GUI")
	xHUD.ShowGUI()
end

function xHUD.SavePositions()
	for i,key in ipairs(xHUD.BarList) do
		local x,y = WindowGetScreenPosition(F[key].name)
		xHUD.Settings[key].x = x
		xHUD.Settings[key].y = y
	end
	for i,key in ipairs(xHUD.IconList) do
		local x,y = WindowGetScreenPosition(F[key].name)
		xHUD.Settings[key].x = x
		xHUD.Settings[key].y = y
		xHUD.Settings[key].scale = WindowGetScale(F[key].name)
	end

	xHUD.SaveBuffPositions()
	xHUD.SaveTextPositions()
end

function xHUD.SetSetting(tab,setting,value)
	if not tab then
		for i,key in ipairs(xHUD.BarList) do
			if type(setting) == "table" then
				if xHUD.Settings[key][setting[1]] then
					xHUD.Settings[key][setting[1]][setting[2]] = value
				end
			else
				xHUD.Settings[key][setting] = value
			end
		end
	elseif tab == "BUFFS" then
		for i,key in ipairs(xHUD.BuffList) do
			xHUD.Settings[key][setting] = value
		end
	else
		if type(setting) == "table" then
			if xHUD.Settings[tab][setting[1]] then
				xHUD.Settings[tab][setting[1]][setting[2]] = value
			end
		else
			xHUD.Settings[tab][setting] = value
		end
	end
end

function xHUD.LoadingEnd(setting)
	if not initialized then
		initialized = true
		xHUD.Recreate()
	end
end

function xHUD.OnUpdate(elapsed)
	if not initialized then return end

	if xHUD.updateCastBar then
		xHUD.curCastTime = xHUD.curCastTime + elapsed
		xHUD.FillBar("xHUDCastBar",xHUD.isChannelCast and (xHUD.maxCastTime-xHUD.curCastTime) or
					 xHUD.curCastTime,xHUD.maxCastTime)
	end

	for k,v in pairs(smoothTable) do
		local B = F[k].Fill

		local x,y = B.width, B.height
		local dx = math.floor(v[2]-v[1])
		local dy = math.floor(v[4]-v[3])

		if dx ~= 0 then
			dx = dx*elapsed/xHUD.Settings.smoothDelay
			if (dx < 0 and x+dx < v[2]) or (dx > 0 and x+dx > v[2]) then
				B:Resize(v[2],y)
				smoothTable[k] = nil
			else
				B:Resize(x+dx,y)
			end
		elseif dy ~= 0 then
			dy = dy*elapsed/xHUD.Settings.smoothDelay
			if (dy < 0 and y+dy < v[4]) or (dy > 0 and y+dy > v[4]) then
				B:Resize(x,v[4])
				smoothTable[k] = nil
			else
				B:Resize(x,y+dy)
			end
		else
			smoothTable[k] = nil
		end

		xHUD.UpdateBarTexture(k)
	end

	updateTime = updateTime + elapsed
	if updateTime > xHUD.Settings.updateDelay then
		xHUD.UpdateBuffTimers(updateTime)
		xHUD.UpdateAlpha()

		local a,b = GetMoraleCooldown(1)
		if b < 2 or a == 0 then
			F["xHUDPlayerMorale"]:Tint(xHUD.Settings["xHUDPlayerMorale"].color2)
		else
			F["xHUDPlayerMorale"]:Tint(xHUD.Settings["xHUDPlayerMorale"].color)
		end

		updateTime = 0
	end

	xHUD.OnUpdateGUI(elapsed)
end

function xHUD.FrameHandle()
	return F
end

function xHUD.Recreate()
	local scale = InterfaceCore.GetScale()
	local B, val

	for _,key in ipairs(xHUD.BarList) do
		if not F[key] then
			F[key] = LibGUI("Window")
			F[key].Background = F[key]("Image")
			F[key].Fill = F[key]("Image")
			F[key].Fill:Layer("secondary")
		end

		B = F[key]
		val = xHUD.Settings[key]

		if val.enabled then
			B:Show()
		else
			B:Hide()
		end
			
		if val.clickthrough then
			B:IgnoreInput()
			B:UnregisterDefaultEvents()
		else
			B:CaptureInput()
			B:RegisterDefaultEvents()
		end
		B.OnRButtonUp = xHUD.OnRButtonUp

		B:Resize(val.width, val.height)
		B.Fill:Resize(val.width, val.height)
		B.Background:Resize(val.width, val.height)
		B:Position(val.x/scale, val.y/scale)

		LayoutEditor.UnregisterWindow(B.name)
		if val.layouteditor then
			LayoutEditor.RegisterWindow(B.name,towstring(key),"",false,false,true)
		end

		B.Fill:Tint(val.color)
		B.Background:Tint(val.bgcolor)
		B.Background:Alpha(val.bgalpha)

		if val.texture and val.texture ~= "none" then
			B.Fill:Texture(val.texture.."FG")
			B.Fill:TexDims(unpack(xHUD.Textures[val.texture]))
			B.Background:Texture(val.texture.."BG")
			B.Background:TexDims(unpack(xHUD.Textures[val.texture]))
		else
			B.Fill:Texture("tint_square")
			B.Background:Texture("tint_square")
		end

		if val.direction == "up" or val.direction == "right" then
			B.Fill:AnchorTo(B,"bottomleft","bottomleft")
		else
			B.Fill:AnchorTo(B,"topright","topright")
		end

	end

	-- career icons
	for _,key in ipairs(xHUD.IconList) do
		if not F[key] then
			F[key] = LibGUI("Image")
		end
		B = F[key]
		val = xHUD.Settings[key]

		B:Parent(F[val.parent])
		B:Resize(64,64)
		B:AnchorTo("Root","topleft","topleft",val.x/scale,val.y/scale)
		B:TexDims(32,32)
		B:Scale(val.scale)
		B:Layer("overlay")
		B:Hide()

		LayoutEditor.UnregisterWindow(B.name)
		if val.layouteditor then
			LayoutEditor.RegisterWindow(B.name,towstring(key),"",false,false,true)
		end
	end


	-- morale bar indicators
	val = xHUD.Settings["xHUDPlayerMorale"]
	local levels = {10,20,50,100}

	B = F["xHUDPlayerMorale"]
	B.Indicators = {}

	for i=1,4 do
		B.Indicators[i] = B("Image")
		B.Indicators[i]:Layer("overlay")
		B.Indicators[i]:Tint(50,50,50)

		-- disable for now
		B.Indicators[i]:Hide()

		if val.direction == "up" then
			B.Indicators[i]:Resize(val.width, 3)
			B.Indicators[i]:AnchorTo(B,"bottom","bottom",0,-val.height * levels[i]/100)
		elseif m_settings.direction == "down" then
			B.Indicators[i]:Resize(val.width, 3)
			B.Indicators[i]:AnchorTo(B,"top","top",0,val.height * levels[i]/100)
		elseif m_settings.direction == "right" then
			B.Indicators[i]:Resize(3, val.height)
			B.Indicators[i]:AnchorTo(B,"left","left",val.width * levels[i]/100,0)
		elseif m_settings.direction == "left" then
			B.Indicators[i]:Resize(3, val.height)
			B.Indicators[i]:AnchorTo(B,"right","right",-val.width * levels[i]/100,0)
		end
	end

	-- influence bar indicators
	val = xHUD.Settings["xHUDPlayerInf"]
	levels = {25,50,100}

	B = F["xHUDPlayerInf"]
	B.Indicators = {}

	for i=1,3 do
		B.Indicators[i] = B("Image")
		B.Indicators[i]:Layer("overlay")
		B.Indicators[i]:Tint(50,50,50)

		-- disable for now
		B.Indicators[i]:Hide()

		if val.direction == "up" then
			B.Indicators[i]:Resize(val.width, 3)
			B.Indicators[i]:AnchorTo(B,"bottom","bottom",0,-val.height * levels[i]/100)
		elseif m_settings.direction == "down" then
			B.Indicators[i]:Resize(val.width, 3)
			B.Indicators[i]:AnchorTo(B,"top","top",0,val.height * levels[i]/100)
		elseif m_settings.direction == "right" then
			B.Indicators[i]:Resize(3, val.height)
			B.Indicators[i]:AnchorTo(B,"left","left",val.width * levels[i]/100,0)
		elseif m_settings.direction == "left" then
			B.Indicators[i]:Resize(3, val.height)
			B.Indicators[i]:AnchorTo(B,"right","right",-val.width * levels[i]/100,0)
		end
	end


	F["xHUDCastBar"]:Hide()
	xHUD.RecreateTexts()
	xHUD.RecreateBuffs()
	xHUD.UpdatePlayerHP()
	xHUD.UpdatePlayerAP()
	xHUD.UpdatePlayerCareer(0,0)
	xHUD.UpdatePlayerMorale(0,0)
	xHUD.UpdatePlayerXP()
	xHUD.UpdatePlayerRP()
	xHUD.UpdatePlayerInf()
	xHUD.UpdateTargets("selffriendlytarget", 0, TargetInfo:UnitType("selffriendlytarget"))
	xHUD.UpdateTargets("selfhostiletarget", 0,	TargetInfo:UnitType("selfhostiletarget"))
end

function xHUD.UpdateBarTexture(frame)
	local settings = xHUD.Settings[frame]
	if not settings.texture or settings.texture == "none" then return end

	local texdims = xHUD.Textures[settings.texture]
	local B = F[frame]
	local x,y = B.width, B.height
	local x2,y2 = B.Fill.width, B.Fill.height

	local dx = x2/x*texdims[1]
	local dy = y2/y*texdims[2]

	B.Fill:TexDims(dx,dy)
	if settings.direction == "up" or settings.direction == "left" then
		B.Fill:Texture(B.Fill.texInfo.texture, texdims[1]-dx, texdims[2]-dy)
	end
end

function xHUD.FillBar(frame,val,max)
	local B = F[frame]
	local x,y = B.width, B.height
	local direction = xHUD.Settings[frame].direction

	if val/max > 1 then
		val = max
	end

	if direction == "right" or direction == "left" then
		x = x*val/max
	else
		y = y*val/max
	end

	if xHUD.Settings[frame].smooth and frame ~= "xHUDCastBar" then
		local x1, y1 = B.Fill.width, B.Fill.height
		smoothTable[frame] = {x1, x, y1, y}
	else
		B.Fill:Resize(x,y)
		xHUD.UpdateBarTexture(frame)
	end
end

function xHUD.UpdatePlayerHP()
	xHUD.FillBar("xHUDPlayerHP",GameData.Player.hitPoints.current,GameData.Player.hitPoints.maximum)
	xHUD.UpdatePlayerHPText()
end

function xHUD.UpdatePlayerAP()
	xHUD.FillBar("xHUDPlayerAP",GameData.Player.actionPoints.current,GameData.Player.actionPoints.maximum)
	xHUD.UpdatePlayerAPText()
end

-- morale points are not part of GameData. We need to remember them.
xHUD.cur_morale = 0
xHUD.max_morale = 3200
xHUD.morale_level = 0

function xHUD.UpdatePlayerMorale(value,level)
	for i=1,level do F["xHUDPlayerMorale"].Indicators[i]:Tint(255,255,255) end
	for i=level+1,4 do F["xHUDPlayerMorale"].Indicators[i]:Tint(50,50,50) end

	xHUD.FillBar("xHUDPlayerMorale",value,100)

	xHUD.cur_morale = value*32
	xHUD.morale_level = level

	xHUD.UpdatePlayerMoraleText()
end

-- career points are not part of GameData. We need to remember them.
xHUD.cur_career = 0
xHUD.max_career = 1

function xHUD.UpdatePlayerCareer(old, new)
	local career = GameData.Player.career.line
	local max = 0
	if career == GameData.CareerLine.BLACK_ORC or career == GameData.CareerLine.SWORDMASTER then
		max = 3

	elseif career == GameData.CareerLine.ARCHMAGE or career == GameData.CareerLine.SHAMAN then
		max = 5
		if new > max then
			new = new - max
			F["xHUDPlayerCareer"].Fill:Tint(xHUD.Settings["xHUDPlayerCareer"].color2)
		else
			F["xHUDPlayerCareer"].Fill:Tint(xHUD.Settings["xHUDPlayerCareer"].color)
		end

	elseif career == GameData.CareerLine.WITCH_HUNTER or career == GameData.CareerLine.ASSASSIN then
		max = 5
		if new == max then
			F["xHUDPlayerCareer"].Fill:Tint(xHUD.Settings["xHUDPlayerCareer"].color3)
		else
			F["xHUDPlayerCareer"].Fill:Tint(xHUD.Settings["xHUDPlayerCareer"].color)
		end

	elseif career == GameData.CareerLine.IRON_BREAKER or career == GameData.CareerLine.SHADE or
			 career == GameData.CareerLine.BRIGHT_WIZARD or career == GameData.CareerLine.SORCERER then
		max = 100
		if new == max then
			F["xHUDPlayerCareer"].Fill:Tint(xHUD.Settings["xHUDPlayerCareer"].color3)
		elseif new >= max/2 then
			F["xHUDPlayerCareer"].Fill:Tint(xHUD.Settings["xHUDPlayerCareer"].color2)
		else
			F["xHUDPlayerCareer"].Fill:Tint(xHUD.Settings["xHUDPlayerCareer"].color)
		end

	elseif career == GameData.CareerLine.SLAYER or career == GameData.CareerLine.CHOPPA then
		max = 100
		if new >= max-25 then
			F["xHUDPlayerCareer"].Fill:Tint(xHUD.Settings["xHUDPlayerCareer"].color3)
		elseif new >= max-75 then
			F["xHUDPlayerCareer"].Fill:Tint(xHUD.Settings["xHUDPlayerCareer"].color2)
		else
			F["xHUDPlayerCareer"].Fill:Tint(xHUD.Settings["xHUDPlayerCareer"].color)
		end

	elseif career == GameData.CareerLine.BLOOD_PRIEST or career == GameData.CareerLine.WARRIOR_PRIEST then
		max = 250
		if new <= max-150 then
			F["xHUDPlayerCareer"].Fill:Tint(xHUD.Settings["xHUDPlayerCareer"].color3)
		elseif new <= max-75 then
			F["xHUDPlayerCareer"].Fill:Tint(xHUD.Settings["xHUDPlayerCareer"].color2)
		else
			F["xHUDPlayerCareer"].Fill:Tint(xHUD.Settings["xHUDPlayerCareer"].color)
		end

	elseif career == GameData.CareerLine.ENGINEER or career == GameData.CareerLine.MAGUS or
			career == GameData.CareerLine.SEER or career == GameData.CareerLine.SQUIG_HERDER then
		max = 100

	else
		d("Not a class with career resources")
		return
	end

	xHUD.cur_career = new
	xHUD.max_career = max

	if xHUD.Settings.xHUDPlayerCareer.enabled then
		F["xHUDPlayerCareer"]:Show()
		xHUD.FillBar("xHUDPlayerCareer", new, max)
		xHUD.UpdatePlayerCareerText()
	end
end

function xHUD.UpdatePlayerXP()
	xHUD.FillBar("xHUDPlayerXP",GameData.Player.Experience.curXpEarned,GameData.Player.Experience.curXpNeeded)
	xHUD.UpdatePlayerExperienceText()
end

function xHUD.UpdatePlayerRP()
	xHUD.FillBar("xHUDPlayerRP",GameData.Player.Renown.curRenownEarned,GameData.Player.Renown.curRenownNeeded)
	xHUD.UpdatePlayerRenownText()
end

xHUD.InfData = nil

function xHUD.UpdatePlayerInf()
	local B = F["xHUDPlayerInf"]
	xHUD.InfData = nil

	for i,val in ipairs(GetAreaData()) do
		if val.influenceID ~= 0 then
			xHUD.InfData = DataUtils.GetInfluenceData(val.influenceID)
		end
	end

	if not xHUD.InfData or not xHUD.Settings.xHUDPlayerInf.enabled then
		B:Hide()
		return
	end

	xHUD.FillBar("xHUDPlayerInf",xHUD.InfData.curValue,xHUD.InfData.rewardLevel[3].amountNeeded)
	B:Show()

	for i=1,3 do
		if xHUD.InfData.curValue >= xHUD.InfData.rewardLevel[i].amountNeeded and
				not xHUD.InfData.rewardLevel[i].rewardsReceived then
			B.Indicators[i]:Tint(255,255,255)
		else
			B.Indicators[i]:Tint(50,50,50)
		end
	end

	xHUD.UpdatePlayerInfluenceText()
end

xHUD.maxCastTime = 0
xHUD.curCastTime = 0
xHUD.curPushBack = 0
xHUD.curSpell = nil
xHUD.isChannelCast = false
xHUD.updateCastBar = false

function xHUD.UpdateCastTime(abilityId, isChannel, desiredCastTime, averageLatency)
	xHUD.maxCastTime = desiredCastTime
	xHUD.curCastTime = 0
	xHUD.curPushBack = 0
	xHUD.isChannelCast = isChannel

	xHUD.curSpell = tostring(GetAbilityData(abilityId).name)

	if desiredCastTime ~= 0 and xHUD.Settings.xHUDCastBar.enabled then
		F["xHUDCastBar"]:Show()
		xHUD.updateCastBar = true
	else
		F["xHUDCastBar"]:Hide()
		xHUD.updateCastBar = false
	end
end

function xHUD.SetBackCastTime(time)
	local t = xHUD.maxCastTime - time
	xHUD.curPushBack = xHUD.curPushBack + t
	xHUD.curCastTime = t
end

function xHUD.UpdateInteractTimer()
	xHUD.maxCastTime = GameData.InteractTimer.time
	xHUD.curCastTime = 0
	xHUD.curSpell = tostring(GameData.InteractTimer.objectName)
end

function xHUD.EndCastTime()
	F["xHUDCastBar"]:Hide()
	xHUD.updateCastBar = false
end

function xHUD.UpdatePetHealth()
	local hp = GameData.Player.Pet.healthPercent

	if hp == 0 then
		F["xHUDPlayerCareer"]:Hide()
	elseif xHUD.Settings["xHUDPlayerCareer"].enabled then
		F["xHUDPlayerCareer"]:Show()
		xHUD.UpdatePlayerCareer(0,hp)
	end
end

local old_target = {}

function xHUD.UpdateTargets(targetClassification, targetId, targetType)
	local fname
	local icon

	if targetClassification == "selffriendlytarget" then
		if xHUD.Settings["xHUDFriendlyTargetIcon"].enabled then 
			fname = "xHUDFriendlyTargetHP"
			icon = "xHUDFriendlyTargetIcon"
		end
	elseif targetClassification == "selfhostiletarget" then
		if xHUD.Settings["xHUDHostileTargetIcon"].enabled then
			fname = "xHUDHostileTargetHP"
			icon = "xHUDHostileTargetIcon"
		end
	else
		return
	end

	if targetType == 0 then
		F[fname]:Hide()
		xHUD.UpdateAlpha()
		xHUD.ClearBuffs(targetClassification)
		old_target[targetClassification] = nil
		return
	end

	TargetInfo:UpdateFromClient()
	local eid = TargetInfo:UnitEntityId(targetClassification)
	if eid ~= old_target[targetClassification] then
		xHUD.ClearBuffs(targetClassification)
		local icondata = Icons.GetCareerIconIDFromCareerLine(TargetInfo:UnitCareer(targetClassification))
		if icondata then
			local texture = GetIconData(icondata)
			F[icon]:Texture(texture)
			F[icon]:Show()
		else
			F[icon]:Hide()
		end
	end
	old_target[targetClassification] = eid

	xHUD.FillBar(fname,TargetInfo:UnitHealth(targetClassification),100)
	if xHUD.Settings[fname].enabled then
		F[fname]:Show()
	end

	xHUD.UpdateTargetText(targetClassification)
	xHUD.UpdateAlpha()
end

function xHUD.OnRButtonUp()
	local frame = SystemData.ActiveWindow.name
	if frame == F["xHUDPlayerHP"].name or frame == F["xHUDPlayerAP"].name then
		PlayerWindow.ShowMenu()
	elseif frame == F["xHUDFriendlyTargetHP"].name then
		PlayerMenuWindow.ShowMenu(towstring(TargetInfo:UnitName("selffriendlytarget")))
	end
end

function xHUD.UpdateAlpha()
	local alpha = 1
	-- only use ooc-alpha if the player is at full health and AP
	local ooc = (not GameData.Player.inCombat) and
				(GameData.Player.hitPoints.current == GameData.Player.hitPoints.maximum) and
				(GameData.Player.actionPoints.current == GameData.Player.actionPoints.maximum)
	local hasTarget = F["xHUDFriendlyTargetHP"]:Showing() or F["xHUDHostileTargetHP"]:Showing()

	for _,key in ipairs(xHUD.BarList) do
		local val = xHUD.Settings[key]

		if ooc and not (val.no_target_fade and hasTarget) then
			if val.ooc_alpha < val.alpha then
				alpha = val.ooc_alpha
			else
				alpha = val.alpha
			end
		else
			alpha = val.alpha
		end

		F[key]:Alpha(alpha)
		WindowSetFontAlpha(F[key].name,alpha)
	end

	for _,key in ipairs(xHUD.BuffList) do
		local val = xHUD.Settings[key]
		for k,v in pairs(xHUD.Buffdata[key]) do
			if ooc and not (val.no_target_fade and hasTarget) then
				if val.ooc_alpha < val.alpha then
					alpha = val.ooc_alpha
				else
					alpha = val.alpha
				end
			else
				alpha = val.alpha
			end

			v.frame:Alpha(alpha)
			WindowSetFontAlpha(v.frame.name,alpha)
		end
	end
end
